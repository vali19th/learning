from collections import defaultdict
from pprint import pprint

from utils.data import has_elements_of_type, is_iterable, is_number, is_str, running_window, sort_keys


def get_substrings(original):
    if is_str(original):
        substrings = defaultdict(list)
        for n in range(2, len(original) + 1):
            for i, substr in enumerate(running_window(original, n)):
                substrings[''.join(substr)].append(i)

        return substrings
    elif is_iterable(original):
        substrings = defaultdict(dict)
        for i, string in enumerate(original):
            for substr, indexes in get_substrings(string).items():
                substrings[substr][i] = indexes

        return {k: v for k, v in substrings.items() if len(v) >= 2}


def get_scores(m, str_1, str_2):
    """Returns the normalized exponential and linear scores a substring match between str_1 and str_2"""
    max_score = max(len(str_1), len(str_2))

    return (
        len(m)**2 / max_score**2,
        len(m) / max_score
    )


def get_fragments(str_1, str_2, *, min_len=1):
    substrings = get_substrings([str_1, str_2])

    # only if found in both strings
    matches = {k: v for k, v in substrings.items() if len(v) == 2}

    # optionally filter by the length of the substring. min_len == 1 -> doesn't filter anything
    matches = {k: v for k, v in matches.items() if len(k) >= min_len}

    # sort the keys to make debugging easier
    matches = sort_keys(matches)

    # calculate linear and exponential scores
    matches_with_scores = ((*get_scores(m, str_1, str_2), m, info[0], info[1]) for m, info in matches.items())

    # the best should be first
    return sorted(matches_with_scores, reverse=True)


def get_matches(str_1, str_2, gap_penalty=None):
    """
    If other is an iterable, it returns a dict with the matches between string and each element in the iterable.

    If gap_penalty is None, return the longest match
    If gap_penalty is a positive number, use fuzzy search.
        0 = no penalty
        0.5 = half the weight of a match of equal length
        1 = same weight as a match of equal length
        2 = double the weight of a match of equal length
        ...
    Otherwise, raise ValueError
    """
    if is_str(str_2):
        matches = get_fragments(str_1, str_2)

        if gap_penalty is None:
            return matches
        elif is_number(gap_penalty) and gap_penalty >= 0:
            pprint(matches)
            x = []
            for s1, s2, m, idx1, idx2 in matches:
                pass

            raise NotImplementedError

            """TODO: find the best combined sequence of matching substrings
            - The order must always be from L to R for both strings
            - the best one is not necessarily the longest. (2c > 1c + 1c + 1c, depends on the gap_penalty)
                - e.g. for '12 345' and '12345': len('12') ** 2 + len('345') ** 2 + len('12345') ** 2 - get_accumulated_gap_penalty(gaps)
            """
        else:
            raise ValueError(f'<gap_penalty> expects None or a positive number. Got {gap_penalty!r}')
    elif is_iterable(str_2) and has_elements_of_type(str_2, str):
        return {str_k: get_matches(str_1, str_k) for str_k in str_2}
    else:
        raise ValueError(f'<other> expects a str or an iterable. Got {str_2!r}')


strings = [
    '123 12345',
    '1234 123',
]

x = get_matches(*strings[:2], gap_penalty=0)
pprint(x)
