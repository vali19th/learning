SRC: https://realpython.com/python-namespace-package/

```python
rm -rf **/*.egg-info
rm -rf venv

python -m venv venv
source venv/bin/activate

python -m pip install -e namespace-pkg-2-dateutil/
python -m pip install -e namespace-pkg-2-magic-numbers/

python snake-service/snake_service.py
```
