# SRC: https://realpython.com/python-practice-problems/#problem-description
# Sum of Integers Up To n
# Write a function, add_it_up(), that takes a single integer as input and returns the sum of the integers from zero to the input parameter.
# The function should return 0 if a non-integer is passed in.


def add_it_up(n):
    if type(n) == int:
        if n < 0:
            return sum(range(0, n-1, -1))
        else:
            return sum(range(n+1))
    else:
        return 0
