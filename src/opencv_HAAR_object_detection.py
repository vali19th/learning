import argparse
import datetime

import cv2

from utils.args import str_or_None
from utils.data import is_int, is_str, xyXY_from_xywh
from utils.datetime_utils import seconds_to_time
from utils.files import delete, mkdir, read
from utils.images import draw_bboxes
from utils.logging_utils import Logger
from utils.sys_utils import shell
from utils.opencv import get_video_images, process_and_show_video, save_video

FILE = __file__.split('/')[-1].split('.')[0]
LOG = Logger(FILE, fh_path=f'logs/{FILE}.log')


def parse_args(argparse_cfg=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='',
    )

    parser.add_argument('--argparse-cfg', type=str, default=argparse_cfg, help='Load argparse config from a file instead of passing the args')
    parser.add_argument('--cleanup', nargs='?', type=bool, default=False, const=True, help='Enable cleanup')
    parser.add_argument('--skip-done', nargs='?', type=bool, default=False, const=True, help='Skip already done items')
    parser.add_argument('--continue-on-error', nargs='?', type=bool, default=False, const=True, help='Continue the main loop if an error occurs')

    parser.add_argument('--IN', type=str, default=f'../data/{FILE}/IN', help='The input')
    parser.add_argument('--OUT', type=str, default=f'../data/{FILE}/OUT', help='The output')
    parser.add_argument('--TMP', type=str, default=f'../data/{FILE}/TMP', help='Tmp dir')

    parser.add_argument('--HAAR', type=str, default=f'../data/{FILE}/face.xml', help='HAAR XML')
    parser.add_argument('--input', type=str_or_None, default=None, help='Input image/video. Use None for webcam')
    parser.add_argument('--scale-factor', type=float, default=1.1, help='HAAR scale factor')
    parser.add_argument('--min-neighbours', type=int, default=3, help='HAAR min neighbours')
    parser.add_argument('--min-size', type=int, default=45, help='HAAR min window size')

    # --HAAR /home/wip/anaconda3/envs/python/lib/python3.10/site-packages/cv2/data/haarcascade_frontalface_alt2.xml --input /home/wip/projects/active/polymore/data/videos/2022-12-13T06_19_31.250Z-TM-42-RTM-13-12-2022-08-18-58.mp4

    if __name__ == '__main__':
        args = parser.parse_args()  # uses sys.argv by default
    else:
        # forcefully ignore sys.argv when calling parse_args() from another module
        args = parser.parse_args([])

    if args.argparse_cfg:
        args.__dict__.update(read(args.argparse_cfg))

    LOG.debug(vars={'args': args.__dict__, 'git_ref': shell('git rev-parse HEAD')})
    return args


def main(ARGS):
    haar = cv2.CascadeClassifier(ARGS.HAAR)

    ARGS.input = ARGS.input or 0
    is_mp4 = (is_str(ARGS.input) and ARGS.input.endswith('.mp4'))
    if is_int(ARGS.input) or is_mp4:
        process_and_show_video(ARGS.input, detect_and_draw, process_fn_args=(haar, ARGS))
    else:
        image = cv2.imread(ARGS.input)
        result = detect_and_draw(ARGS, image, haar)
        cv2.imshow('Faces', result)
        cv2.waitKey(0)


def detect_and_draw(image, haar, ARGS):
    bboxes = detect(image, haar, ARGS)
    return draw_bboxes(image, bboxes)


def detect(image, haar, ARGS):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = haar.detectMultiScale(
        gray,
        scaleFactor=ARGS.scale_factor,
        minNeighbors=ARGS.min_neighbours,
        minSize=(ARGS.min_size, ARGS.min_size)
    )

    return [xyXY_from_xywh(xywh) for xywh in faces]


def setup(ARGS):
    cleanup(ARGS)
    if not ARGS.skip_done:
        delete(ARGS.OUT)

    mkdir(ARGS.IN)
    mkdir(ARGS.TMP)
    mkdir(ARGS.OUT)


def cleanup(ARGS):
    delete(ARGS.TMP)


if __name__ == '__main__':
    start = datetime.datetime.now()
    ARGS = parse_args()

    setup(ARGS)
    main(ARGS)
    cleanup(ARGS)

    end = datetime.datetime.now()
    duration = seconds_to_time((end - start).seconds)
    LOG.debug(f'{start} -> {end} | {duration}')
