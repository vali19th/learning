import itertools
import time
import datetime
from math import sqrt

import asyncio
import threading
import multiprocessing

now = datetime.datetime.now


def measure_duration(f):
    def wrapper(*args):
        start = time.perf_counter()
        r = f(*args)
        end = time.perf_counter()
        print(f'Ran {f.__name__}{args} in {round((end - start) * 1000)}ms')
        return r

    return wrapper


def log_time(f):
    def wrapper(*args):
        start = time.perf_counter()
        print(f'[{now()}] start {f.__name__}{args}')
        r = f(*args)
        end = time.perf_counter()
        print(f'[{now()}] end {f.__name__}{args} -> {round((end - start) * 1000)}ms')
        return r

    return wrapper


# @log_time
def f(s, sleep):
    """\
    sleep = True -> just sleep for `s` seconds
    sleep = False -> do some computation for `s` seconds
    """
    if sleep:
        time.sleep(s)
    else:
        start = time.perf_counter()
        while (time.perf_counter() - start) < s:
            sqrt(2)


# @log_time
async def f_async(s, sleep):
    f(s, sleep)


@measure_duration
def with_multiprocessing(seconds, sleep):
    threads = []
    for x in seconds:
        threads.append(multiprocessing.Process(target=f, args=(x, sleep)))
        threads[-1].start()

    for t in threads:
        t.join()


@measure_duration
def with_multithreading(seconds, sleep):
    threads = []
    for x in seconds:
        threads.append(threading.Thread(target=f, args=(x, sleep)))
        threads[-1].start()

    for t in threads:
        t.join()


@measure_duration
def with_asyncio(seconds, sleep):
    async def iterate():
        tasks = []
        for x in seconds:
            tasks.append(loop.create_task(f_async(x, sleep)))

        await asyncio.wait(tasks)

    loop = asyncio.new_event_loop()
    loop.run_until_complete(iterate())
    loop.close()


seconds = [0.1, 0.1, 0.1, 1]
seconds_r = list(reversed(seconds))

for secs, sleep in itertools.product([seconds, seconds_r], [True, False]):
    with_multiprocessing(secs, sleep)
    with_multithreading(secs, sleep)
    with_asyncio(secs, sleep)
    print()
