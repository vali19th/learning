from itertools import product
from pprint import pprint

from utils.misc import compare_strings, transpose


def sudoku(problem):
    _preconditions(problem)
    # M1: https://norvig.com/sudoku.html
    # M2: https://realpython.com/python-practice-problems/#python-practice-problem-5-sudoku-solver

    solution = problem.replace('0', '1').replace('.', '1')
    _postconditions(problem, solution)
    return solution


def _preconditions(problem):
    if not isinstance(problem, str):
        raise TypeError(f'Expected a str, got {type(problem).__name__!r} instead')

    if len(problem) != 81:
        raise ValueError(f'Expected 81 dots and digits, got {len(problem)} instead')

    if not set(problem).issubset('.0123456789'):
        mistakes = ''.join(c if c in '.0123456789' else f'[{c}]' for c in problem)
        raise ValueError(f'Expected only dots and digits, got {mistakes!r} instead')


def _postconditions(problem, solution):
    def _matches_all_problem_chars(problem, solution):
        def _match_char(p, s):
            return p == s or p in '.0'

        return all(_match_char(p, s) for p, s in zip(problem, solution))

    def _diff_fn(p, s):
        return p in '.0' or p == s

    assert isinstance(solution, str)
    if solution != 'unsolvable':
        assert len(solution) == 81
        assert set(solution).issubset('123456789')
        assert _matches_all_problem_chars(problem, solution), compare_strings(problem, solution, _diff_fn)


def _get_rows(chars):
    return [chars[i*9:(i+1)*9] for i in range(9)]


def _get_cols(chars):
    rows = _get_rows(chars)
    matrix = [list(row) for row in rows]
    return [''.join(col) for col in transpose(matrix)]


def _get_boxes(chars):
    rows = _get_rows(chars)
    boxes = [[row[i*3:(i+1)*3] for row in rows] for i in range(3)]
    boxes = [''.join(b[i*3:(i+1)*3]) for i, b in product(range(3), boxes)]
    return boxes


def get_grid(chars):
    grid = []
    for i, row in enumerate(_get_rows(chars)):
        tmp = '|'.join(row[j*3:(j+1)*3] for j in range(3))
        grid.append(' '.join(tmp))
        if i in [2, 5]:
            grid.append('------+------+------')

    return '\n'.join(grid)
