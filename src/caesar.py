"""
Caesar Cipher (caesar.py)

A Caesar cipher is a simple substitution cipher in which each letter of the plain text is substituted with a letter found by moving n places down the alphabet. For example, assume the input plain text is the following:

abcd xyz
If the shift value, n, is 4, then the encrypted text would be the following:

efgh bcd
You are to write a function that accepts two arguments, a plain-text message and a number of letters to shift in the cipher. The function will return an encrypted string with all letters transformed and all punctuation and whitespace remaining unchanged.

Note: You can assume the plain text is all lowercase ASCII except for whitespace and punctuation.
"""
import string
from pprint import pprint


def caesar(text, n):
    def shift(c, n):
        if c.isalpha():
            letters = string.ascii_lowercase
            i = letters.index(c)
            i = (i + n) % len(letters)
            return letters[i]
        else:
            return c

    return ''.join(shift(c, n) for c in text)


"""The RealPython solution
def caesar(plain_text, shift_num=1):
    letters = string.ascii_lowercase
    mask = letters[shift_num:] + letters[:shift_num]
    trantab = str.maketrans(letters, mask)
    return plain_text.translate(trantab)
"""
