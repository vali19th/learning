"""
Accepts a string representing the content of a Linux-like log file. Mixed in among the various statements are messages indicating the state of the device. They look like this:

Jul 11 16:11:51:490 [139681125603136] dut: Device State: ON
The device state message has many possible values, but this program cares about only three: ON, OFF, and ERR.

Your program will parse the given log file and print out a report giving how long the device was ON and the timestamp of any ERR conditions.

Example:
Device was on for 7 seconds
Timestamps of error events:
    Jul 11 16:11:54:661
    Jul 11 16:11:56:067
"""
import datetime
import re


def parse_log(log):
    lines = log.strip().splitlines()
    parsed = [(line, _parse_line(line)) for line in lines]

    for i, (line, match) in enumerate(parsed):
        _check_format(line, match)
        parsed[i] = (line, match.groups())

    events = _get_interesting_states(parsed)
    if not events:
        return ''

    _check_initial_state(events)
    errors = _get_errors(events)
    duration = _get_duration(events)
    if errors:
        return duration + '\n' + errors
    else:
        return duration


def _get_duration(events):
    toggles = _get_toggles(events)
    deltas = _get_deltas(toggles)

    if deltas:
        return f'Device was on for {sum(deltas)}s'
    else:
        start_dt, _ = events[0]
        return f'The device has been running without interruption since {start_dt}'


def _get_deltas(toggles):
    deltas = []
    for (t1, s1), (t2, s2) in zip(toggles, toggles[1:]):
        _check_alternating_toggles(s1, s2, t1, t2)
        t1 = datetime.datetime.strptime(t1, '%H:%M:%S')
        t2 = datetime.datetime.strptime(t2, '%H:%M:%S')
        deltas.append((t2 - t1).seconds)

    return deltas


def _get_toggles(events):
    return [(time, state) for time, state in events if state in ['ON', 'OFF']]


def _get_errors(events):
    errors = [time for time, state in events if state == 'ERR']
    if errors:
        return 'Timestamps of error events:' + ''.join(f'\n    {time}' for time in errors)
    else:
        return ''


def _get_interesting_states(parsed):
    return [(time, state) for _, (time, state) in parsed if state in ['ON', 'OFF', 'ERR']]


def _check_format(line, match):
    if match is None:
        raise ValueError(f'Unrecognized format: {line!r}')


def _check_alternating_toggles(s1, s2, t1, t2):
    if s1 == s2:
        raise ValueError(f'Found two consecutive "{s1}" toggles at {t1} and {t2}')


def _check_initial_state(events):
    _, initial_state = events[0]
    if initial_state != 'ON':
        raise ValueError(f'The initial state must always be "ON", instead got {initial_state!r}')


def _parse_line(line):
    return re.search(r'^([0-9]{2}:[0-9]{2}:[0-9]{2}): Device State: ([A-Z]+)$', line)
