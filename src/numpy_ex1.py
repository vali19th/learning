import itertools
from pprint import pprint
import numpy as np


def main():
    # SRC: https://www.labri.fr/perso/nrougier/from-python-to-numpy/#id2

    test(np.arange(100))
    test(np.arange(100).reshape(2, -1))
    test(np.arange(100).reshape(2, -1, 2))
    test(np.arange(100).reshape(2, 2, -1))


def test(base):
    for start, stop, step in itertools.product(range(0, 12), range(0, 12), range(-11, 12)):
        if step == 0:
            continue

        view = base[start:stop:step]
        start_2, stop_2, step_2 = calc_start_stop_step_of_view(base, view)

        view_2 = base[start_2:stop_2:step_2]
        assert np.allclose(view_2, view), (view_2, view)


def calc_start_stop_step_of_view(base, view):
    offset_start = np.byte_bounds(view)[0] - np.byte_bounds(base)[0]
    offset_stop = np.byte_bounds(view)[-1] - np.byte_bounds(base)[-1]

    start = offset_start // base.itemsize
    stop = base.size + offset_stop // base.itemsize

    ratio = base.strides[0] // base.strides[-1]
    stop //= ratio

    step = view.strides[0] // base.strides[0]
    if step < 0:
        start, stop = stop - 1, start - 1

    if start < 0:
        start = base.size - start

    if stop < 0:
        stop = base.size - stop

    return (start, stop, step)


def calc_start_stop_step_of_view_1D(base, view):
    offset_start = np.byte_bounds(view)[0] - np.byte_bounds(base)[0]
    offset_stop = np.byte_bounds(view)[-1] - np.byte_bounds(base)[-1]

    start = offset_start // base.itemsize
    stop = base.size + offset_stop // base.itemsize

    step = view.strides[0] // base.strides[0]
    if step < 0:
        start, stop = stop - 1, start - 1

    if start < 0:
        start = base.size - start

    if stop < 0:
        stop = base.size - stop

    return (start, stop, step)


if __name__ == "__main__":
    main()
