import datetime
import functools
import pickle
import time
import traceback
import wrapt

from utils.sys_utils import shell


def try_catch(f=None, catch=[Exception]):
    if f is None:
        return functools.partial(try_catch, catch=catch)
    elif type(f).__name__ != 'function':
        return functools.partial(try_catch, catch=f)

    @wrapt.decorator
    def inner(wrapped, instance, args, kwargs):
        try:
            return wrapped(*args, **kwargs)
        except Exception as e:
            stop = True
            if e == catch or type(e) == catch:
                stop = False
            elif catch == Exception or catch == [Exception] or catch == 'Exception' or catch == ['Exception']:
                stop = False
            elif type(catch) == str:
                if type(e).__name__ == catch or str(e) == catch:
                    stop = False
            elif hasattr(catch, '__iter__'):
                if e in catch or type(e).__name__ in catch or str(e) in catch:
                    stop = False
                else:
                    for ex in catch:
                        if type(ex) != str:
                            if e == ex or isinstance(e, ex):
                                stop = False
                                break

        stacktrace = 'Traceback (most recent call last):'
        stacktrace += '\n' + ''.join(traceback.format_stack()[:-1])[:-1]
        stacktrace += '\n'.join(traceback.format_exc().split('\n')[-4:])[:-1]
        print(stacktrace)

        if stop:
            exit()

    return inner(f)


def dump(f):
    F = f.__code__
    arg_names = F.co_varnames
    n_pos = F.co_argcount
    n_kw = F.co_kwonlyargcount
    arg_pos = arg_names[:n_pos]
    arg_kw = arg_names[n_pos:n_pos + n_kw]

    def wrapper(*args,**kwargs):
        if f.__kwdefaults__:
            parameters = {**f.__kwdefaults__, **kwargs, **dict(zip(arg_pos, args))}
        else:
            parameters = {**kwargs, **dict(zip(arg_pos, args))}

        if len(args) > n_pos:
            parameters['args'] = args[n_pos:]

        output = f(*args, **kwargs)

        path = f.__module__ + '_' + f.__qualname__
        dt = str(datetime.datetime.now()).replace(':', '.').replace(' ', '_')
        out_dir = f'tests/data/{path}'

        info = {
            'path': path,
            'input': parameters,
            'output': output
        }

        shell(f'mkdir -p {out_dir}')
        with open(f'{out_dir}/{dt}.pkl', 'wb') as pkl:
            pickle.dump(info, pkl, 4)

        return output

    return wrapper


def debug(logger, log_args=False, log_result=False, log_duration=False):
    @wrapt.decorator
    def inner(wrapped, instance, args, kwargs):
        if log_duration:
            t0 = time.perf_counter()

        vars = {'@args': args, '@kwargs': kwargs} if log_args else {}
        logger.debug(f'ENTER {wrapped.__name__}', vars=vars)
        result = wrapped(*args, **kwargs)

        vars = {}
        if log_duration: vars['@duration'] = time.perf_counter() - t0
        if log_result: vars['@result'] = result
        logger.debug(f'EXIT {wrapped.__name__}', vars=vars)

        return result

    return inner


def vectorize(f):
    @functools.wraps(f)
    def new_f(first_arg, *args, **kwargs):
        if hasattr(first_arg, '__iter__'):
            if str(type(first_arg)) == "<class 'numpy.ndarray'>":
                import numpy as np
                return np.array([f(a, *args, **kwargs) for a in first_arg])
            else:
                return type(first_arg)(f(a, *args, **kwargs) for a in first_arg)
        else:
            return f(first_arg, *args, **kwargs)

    return new_f


def vectorize_iter(f):
    @functools.wraps(f)
    def new_f(first_arg, *args, **kwargs):
        if hasattr(first_arg, '__iter__'):
            if str(type(first_arg)) == "<class 'numpy.ndarray'>":
                import numpy as np
                return np.array([f(a, *args, **kwargs) for a in first_arg])
            else:
                return (f(a, *args, **kwargs) for a in first_arg)
        else:
            return f(first_arg, *args, **kwargs)

    return new_f
