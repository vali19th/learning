import re
from pprint import pprint

import numpy as np
from detectron2.structures import BoxMode
from fastai.vision.all import load_learner

from detectron2.data.datasets.coco import register_coco_instances
from detectron2.config.config import get_cfg
from detectron2.engine.defaults import DefaultPredictor
from detectron2.utils.visualizer import ColorMode, Visualizer

from utils.data import is_dict
from utils.files import read
from utils.images import draw_bboxes, load_image, merge_image_with_masks, show_image


def get_bounding_boxes(img_path, json_path):
    '''
    SRC: https://github.com/facebookresearch/detectron2/issues/50#issuecomment-542068981
    AD Oct 2019 : loading function to calculate the bboxes from the masks (as it does not exist so far in Detectron2)
    expected format:

    {'file_name': 'path/to/image',
    'height' : imgheight,
    'width': imgwidth,
    'annotations':
    [{'bbox': [xmin,ymin,xmax,ymax], 'bbox_mode': <BoxMode.XYXY_ABS: 0>,
    'segmentation': [[polygon/coordinates/x/y]],
    'category_id': 0, 'iscrowd': 0},
    {'bbox': [xmin2,ymin2,xmax2,ymax2], 'bbox_mode': <BoxMode.XYXY_ABS: 0>,
    'segmentation': [[polygon2/coordinates/x/y]],
    'category_id': 0, 'iscrowd': 0},

    etc ...]

    }
    INPUTS:
        img_path . str, path to the images
        json_path. str, path to the json annotation file (coco format)

    '''

    imgs_anns = read(json_path)

    dataset_dicts = []
    for img in imgs_anns['images']:
        record = {}

        filename = img_path + '/' + img['file_name']
        height, width = load_image(filename).shape[:2]
        image_id = img['id']

        record["file_name"] = filename
        record["height"] = height
        record["width"] = width

        for annos in imgs_anns['annotations']:
            if annos['image_id'] == image_id:

                objs = []
                poly = annos['segmentation']

                for p in poly:
                    bbox=[]
                    x, y = p[::2] , p[1::2]
                    bbox.append(np.min(x))
                    bbox.append(np.min(y))
                    bbox.append(np.max(x))
                    bbox.append(np.max(y))
                    obj = {"bbox": bbox,
                            "bbox_mode": BoxMode.XYXY_ABS,
                            "segmentation": [p],
                            "category_id": 0,
                            "iscrowd": 0}
                    objs.append(obj)

        record["annotations"] = objs
        dataset_dicts.append(record)

    return dataset_dicts


class Classifier:
    def __init__(self, pkl, cpu=True):
        self._learn = load_learner(pkl, cpu=cpu)
        self._learn.eval()

    def __call__(self, image):
        """sample can be either the path to an image or the image itself"""
        image = load_image(image)
        with self._learn.no_bar():
            result = self._learn.predict(image)

        confidences = [x.item() for x in result[2]]
        classes = self._learn.dls.vocab
        classes_and_confidences = dict(zip(classes, confidences))

        return {
            'image': image,
            'class': result[0],
            'confidence': classes_and_confidences[result[0]],
            'classes': classes_and_confidences
        }


class CustomDetectron:
    def __init__(self, model_weights, threshold, num_classes, model_yaml, cpu=True, classes=None):
        try:
            register_coco_instances('_', {}, '', '')
        except AssertionError as e:
            if not re.search('Dataset .* is already registered!', str(e)):
                raise e

        self._cfg = get_cfg()

        if classes is None:
            self._classes = None
        elif is_dict(classes):
            self._classes = classes
        else:
            self._classes = {int(id): cls for id, cls in read(classes).items()}

        self._cpu = cpu
        if self._cpu:
            self._cfg.MODEL.DEVICE = 'cpu'

        self._cfg.merge_from_file(model_yaml)
        self._cfg.DATALOADER.NUM_WORKERS = 2
        self._cfg.MODEL.ROI_HEADS.NUM_CLASSES = num_classes
        self._cfg.MODEL.WEIGHTS = model_weights
        self._cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = threshold
        self._learn = DefaultPredictor(self._cfg)

    def __call__(self, image, keep_instances=False):
        image = load_image(image, 'RGB')

        outputs = self._learn(image)
        rez = outputs['instances'].get_fields()

        new_dict = {
            'scores': rez['scores'].cpu().numpy().tolist(),
            'bboxes': rez['pred_boxes'].tensor.cpu().numpy().tolist(),
            'class_numbers': rez['pred_classes'].cpu().numpy().tolist(),
            'masks': rez['pred_masks'].cpu().numpy().tolist(),
            'image': image
        }

        if self.classes:
            new_dict['classes'] = [self.classes[i] for i in new_dict['class_numbers']]

        if keep_instances:
            # this is needed if you want to call self.visualize()
            new_dict['instances'] = outputs['instances']

        return new_dict

    @property
    def cpu(self):
        return self._cpu

    @property
    def classes(self):
        return self._classes

    def mask_object(self, out_dict):
        v = Visualizer(out_dict['image'][:, :, ::-1], scale=0.6, instance_mode=ColorMode.IMAGE_BW)
        v = v.draw_instance_predictions(out_dict['instances'].to(self._cfg.MODEL.DEVICE))
        return v.get_image()[:, :, ::-1]

    def visualize(self, data, with_bbox=True, with_segmentation=True, custom=False):
        if custom:
            image = data['image']
            if with_bbox:
                image = draw_bboxes(image, data['bboxes'], in_place=True)

            if with_segmentation:
                image = merge_image_with_masks(image, np.array(data['masks']), alpha=0.25, in_place=True)
        else:
            image = self.mask_object(data)

        show_image(image)
