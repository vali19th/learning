import pytest

from utils.misc import compare_strings


def check_error(e, message):
    actual = e.value.args[0].replace('\'', '"')
    expected = message.replace('\'', '"')
    assert actual == expected, compare_strings(actual, expected)


def raises(error, f, *args, **kwargs):
    try:
        with pytest.raises(type(error)) as e:
            f(*args, **kwargs)
        actual = e.value
    except Exception as e:
        actual = e

    if isinstance(error, Exception):
        actual = _repr(actual)
        error = _repr(error)
        assert actual == error, compare_strings(actual, error)


def _repr(error):
    msg = str(error).replace('\'', '"').replace('\\"', '"')
    return f'{type(error).__name__}(\'{msg}\')'
