import ast
import inspect
import json
import os
import re
import html
import logging
import traceback
import warnings
from collections import defaultdict
from logging import handlers

import xmltodict

from utils.files import read, mkdir
from utils.data import (
    dict_from_nested_ODict,
    is_dict,
    is_primitive, pretty_repr,
    is_list_or_tuple
)


class Logger:
    def __init__(
        self,
        name,
        fh_path=None,
        fh_level=logging.DEBUG,
        fh_format='<%(levelname)s dt="%(asctime)s" %(message)s\n</%(levelname)s>\n\n\n',
        fh_rotation='midnight',
        sh=True,
        sh_level=logging.DEBUG,
        sh_format='<%(levelname)s dt="%(asctime)s" %(message)s\n</%(levelname)s>\n'
    ):
        """The format is missing some info that I can only add correctly only through the _prepare_log() function"""
        self._logger = logging.getLogger(name)

        # Check if a handler with the same name has already been instantiated and it has handlers already added
        # In that case, logging.getLogger(name) is referencing the existing one, not creating a new logger
        has_fh = has_sh = False
        if self._logger.hasHandlers():
            has_fh = any(type(h) == handlers.TimedRotatingFileHandler for h in self._logger.handlers)
            has_sh = any(type(h) == logging.StreamHandler for h in self._logger.handlers)

        self._logger.setLevel(min(fh_level, sh_level))

        if fh_path and not has_fh:
            path_dir = fh_path.rpartition('/')[0]
            mkdir(path_dir)
            if fh_rotation:
                file_handler = handlers.TimedRotatingFileHandler(fh_path, when=fh_rotation)
            else:
                file_handler = logging.FileHandler(fh_path)

            file_handler.setFormatter(logging.Formatter(fh_format, "%Y-%m-%d %H:%M:%S"))
            file_handler.setLevel(fh_level)
            self._logger.addHandler(file_handler)

        if sh and not has_sh:
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(logging.Formatter(sh_format, "%Y-%m-%d %H:%M:%S"))
            stream_handler.setLevel(sh_level)
            self._logger.addHandler(stream_handler)

    @classmethod
    def _prepare_log(self, *args, message='', exception=None, vars=None):
        vars = vars if vars else {}
        message = message_from_args(args) if args else message
        sections = []

        if exception: sections.append(f'<stacktrace>\n{get_stacktrace(exception)}</stacktrace>')
        if vars: sections.append('<vars>' + pretty_repr(vars) + '</vars>')
        if message: sections.append(f'<message>{message}</message>')

        file, func, line = get_file_func_line(depth=3)
        missing_format_info = f'file="{file}" func="{func}" line="{line}">\n'
        return missing_format_info + '\n'.join(sections)

    def __call__(self, *args, **kwargs):  self.info(*args, **kwargs)
    def debug(self, *args, **kwargs):     self._logger.debug(self._prepare_log(*args, **kwargs))
    def info(self, *args, **kwargs):      self._logger.info(self._prepare_log(*args, **kwargs))
    def warning(self, *args, **kwargs):   self._logger.warning(self._prepare_log(*args, **kwargs))
    def error(self, *args, **kwargs):     self._logger.error(self._prepare_log(*args, **kwargs))
    def exception(self, *args, **kwargs): self._logger.exception(self._prepare_log(*args, **kwargs))


def get_file_func_line(depth=1):
    file = os.path.relpath(inspect.stack()[depth].filename)
    line = inspect.stack()[depth].lineno
    func = inspect.stack()[depth].function
    return file, func, line


def message_from_args(args):
    if len(args) == 0:
        return ''
    elif len(args) == 1 and is_primitive(args[0]):
        return args[0]
    else:
        return repr(args)


def get_stacktrace(e):
    return ''.join(traceback.format_exception(None, e, e.__traceback__))


def parse_log(path):
    log = read(path, as_type='txt')

    # Return an empty dict instead of None
    if log == '':
        return {}

    # escape everything inside my custom tags to prevent an error in xmltodict.parse()
    log = re.sub(r'<(message|vars|stacktrace)>((?:\n.*?)*?)</\1>', lambda m: f'<{m.group(1)}>{html.escape(m.group(2))}</{m.group(1)}>', log)

    # add a fake root to prevent an error in xmltodict.parse()
    log = f'<ROOT>{log}</ROOT>'

    log = xmltodict.parse(log)['ROOT']  # prase, then throw out the fake <ROOT>
    log = dict_from_nested_ODict(log)  # OrderedDict gets in my way, so convert it to dict

    # standardise the result. Add the children tags to a list even if it's just on
    log = {level: data if is_list_or_tuple(data) else [data] for level, data in log.items()}

    return log


def extract_error_from_stacktrace(stack):
    for line in stack.split('\n')[::-1]:
        try:
            return re.search(r'.*(Error|psycopg2.errors|ftplib.error_perm).*', line).group(0)
        except Exception:
            pass


def group_errors(log):
    errors = defaultdict(list)
    for x in log['ERROR']:
        x_vars = x.get('vars', {})
        if is_dict(x_vars):
            # encoded with json.dumps() => decode with json.loads()
            variables = json.loads(x_vars)
        else:
            # encoded with repr() => decode with ast.literal_eval()
            variables = {k: ast.literal_eval(v) for k, v in re.findall(r"(\w+) = (.*)", x_vars)}

        errors[x['stacktrace']].append({
            'timestamp': x.get('@timestamp').ljust(26, '0'),
            'filename': x.get('@file'),
            'funcname': x.get('@function', 'main'),
            'linenum': x.get('@line'),
            'message': x.get('message', ''),
            'git_ref': x.get('meta', {}).get('git_ref', 'c797d433f21b8f228718390e3699244a5c0cbfff'),
            'error_repr': extract_error_from_stacktrace(x['stacktrace']),
            'vars': variables,
        })

    return errors


def warn(message):
    file, func, line = get_file_func_line(2)
    warnings.warn_explicit(f'{func}() -> {message}', RuntimeWarning, file, line)
