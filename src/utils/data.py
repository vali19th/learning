import copy
import inspect
import itertools
import math
import random
import json
import re
from collections import defaultdict
from copy import deepcopy

from pprint import pprint


def is_mpl_figure(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import matplotlib as mpl
        return isinstance(x, mpl.figure.Figure)
    except Exception:
        return False


def is_PIL_Image(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        from PIL import Image
        return isinstance(x, Image.Image)
    except Exception:
        return False


def is_tensor(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import torch
        return torch.is_tensor(x)
    except Exception:
        return False


def is_numpy_array(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import numpy as np
        return isinstance(x, np.ndarray)
    except Exception:
        return False


def is_numpy_primitive(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import numpy as np
        return isinstance(x, (np.generic, np.str, np.bool))
    except Exception:
        return False


def is_numpy_number(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import numpy as np
        return isinstance(x, np.generic)
    except Exception:
        return False


def is_BytesIO(x):
    # import only if you call this function.
    try:
        from io import BytesIO
        return isinstance(x, type(BytesIO()))
    except Exception:
        return False


def is_pandas_df(x):
    # import only if you call this function. This avoids having the package as a dependency
    try:
        import pandas as pd
        return isinstance(x, pd.core.frame.DataFrame)
    except Exception:
        return False


def is_bytes(x): return isinstance(x, type(b''))


def is_float(x): return isinstance(x, float)


def is_int(x): return isinstance(x, int)


def is_number(x): return is_float(x) or is_int(x)


def is_str(x): return isinstance(x, str)


def is_bool(x): return x is True or x is False


def is_primitive(x): return x is None or is_number(x) or is_str(x) or is_bool(x)


def is_list_or_tuple(x): return isinstance(x, (list, tuple))


def is_list(x): return isinstance(x, list)


def is_tuple(x): return isinstance(x, tuple)


def is_set(x): return isinstance(x, set)


def is_dict(x): return isinstance(x, dict)


def is_dict_keys(x): return isinstance(x, type({}.keys()))


def is_dict_values(x): return isinstance(x, type({}.values()))


def is_dict_items(x): return isinstance(x, type({}.items()))


def is_dict_view(x): return is_dict_keys(x) or is_dict_values(x) or is_dict_items(x)


def is_empty(x):
    assert is_sizeable(x)

    if is_numpy_array(x):
        return not (x.ndim and x.size)
    else:
        return len(x) == 0


def is_not_empty(x):
    return not is_empty(x)


def has_len(x, n):
    return is_iterable(x) and len(x) == n


def has_elements_of_type(x, elements):
    if not is_iterable(x):
        return False

    if isinstance(elements, type):
        return all(isinstance(xi, elements) for xi in x)

    return all(map(elements, x))


def is_2D(x): return has_len(x, 2) and has_elements_of_type(x, is_number)


def is_3D(x): return has_len(x, 3) and has_elements_of_type(x, is_number)


def is_4D(x): return has_len(x, 4) and has_elements_of_type(x, is_number)


def is_line(x, elements=is_2D): return has_len(x, 2) and has_elements_of_type(x, elements)


def is_bbox_diagonal(data): return is_line(data)


def is_line_with_width(data): return is_iterable(data) and is_line(data[0]) and is_number(data[1])


def is_polygon(data): return is_iterable(data) and len(data) == 4 and all(is_2D(p) for p in data)


def is_iterable(x): return hasattr(x, '__iter__') and not is_str(x)


def is_sizeable(x): return hasattr(x, '__len__')


def is_callable(x): return hasattr(x, '__call__')


def is_indexable(x): raise NotImplementedError


def is_sequence(x): raise NotImplementedError


def is_notebook():
    try:
        get_ipython
        return True
    except NameError:
        return False


def maybe_str(value):
    if value == 'None':
        return None
    elif is_str(value):
        return value
    else:
        raise TypeError(f'Expected a string. Got a {repr(type(value))} instead')


def has_keyword_arg_strict(f):
    return any(p.kind == p.KEYWORD_ONLY for p in inspect.signature(f).parameters.values())


def has_positional_arg_strict(f):
    return any(p.kind == p.POSITIONAL_ONLY for p in inspect.signature(f).parameters.values())


def has_positional_or_keyword_arg_strict(f):
    return any(p.kind == p.POSITIONAL_OR_KEYWORD for p in inspect.signature(f).parameters.values())


def has_keyword_arg(f):
    return has_keyword_arg_strict(f) or has_positional_or_keyword_arg_strict(f)


def has_positional_arg(f):
    return has_positional_arg_strict(f) or has_positional_or_keyword_arg_strict(f)


def has_positional_or_keyword_arg(f):
    return has_keyword_arg(f) or has_positional_arg(f)


def has_var_keyword_arg(f):
    return any(p.kind == p.VAR_KEYWORD for p in inspect.signature(f).parameters.values())


def has_var_positional_arg(f):
    return any(p.kind == p.VAR_POSITIONAL for p in inspect.signature(f).parameters.values())


def is_keyword_arg_strict(f, arg):
    parameter = inspect.signature(f).parameters.get(arg)
    return parameter.kind == parameter.KEYWORD_ONLY


def is_positional_arg_strict(f, arg):
    parameter = inspect.signature(f).parameters.get(arg)
    return parameter.kind == parameter.POSITIONAL_ONLY


def is_positional_or_keyword_arg_strict(f, arg):
    parameter = inspect.signature(f).parameters.get(arg)
    return parameter.kind == parameter.POSITIONAL_OR_KEYWORD


def is_keyword_arg(f, arg):
    return is_keyword_arg_strict(f, arg) or is_positional_or_keyword_arg_strict(f, arg)


def is_positional_arg(f, arg):
    return is_keyword_arg_strict(f, arg) or is_positional_or_keyword_arg_strict(f, arg)


def is_positional_or_keyword_arg(f, arg):
    return is_keyword_arg(f, arg) or is_positional_arg(f, arg)


def is_var_keyword_arg(f, arg):
    parameter = inspect.signature(f).parameters.get(arg)
    return parameter.kind == parameter.POSITIONAL_OR_KEYWORD


def is_var_positional_arg(f, arg):
    parameter = inspect.signature(f).parameters.get(arg)
    return parameter.kind == parameter.POSITIONAL_OR_KEYWORD


def arg_in_function(arg, f):
    return inspect.signature(f).parameters.get(arg) is not None


def get_arg_type(f, arg, as_str=True):
    arg_type = inspect.signature(f).parameters.get(arg).kind
    return str(arg_type) if as_str else arg_type


def get_arg_default_value(f, arg): raise NotImplementedError


def arg_has_default_value(f, arg): raise NotImplementedError


def keep_args_accepted_by_function(f, args):
    """For now it only covers the case for normal keyword args and **kwargs.
    TODO: positional only and *args
    """

    if has_var_keyword_arg(f):
        return args
    else:
        return {arg: value for arg, value in args.items()
                if arg_in_function(arg, f) and is_keyword_arg(f, arg)}


# data ==============================================================================================
def dict_from_nested_ODict(x):
    if is_dict(x):
        return {k: dict_from_nested_ODict(v) for k, v in x.items()}
    elif is_iterable(x):
        return type(x)(dict_from_nested_ODict(xi) for xi in x)
    else:
        return x


def pretty_repr(d, *, sort_keys=True, indent=2):
    from utils.datetime_utils import is_datetime_or_date_or_time
    d = depth_map(str, d, condition=is_datetime_or_date_or_time)
    return json.dumps(d, sort_keys=sort_keys, indent=indent)


def insert_ranks(items, columns=None):
    if columns is None:
        if items:
            first = list(items.values())[0] if is_dict(items) else items[0]
            assert is_iterable(first)
            columns = first.keys() if is_dict(first) else range(len(first))
        else:
            # `items` is an empty data structure so there's nothing to do. Return it as it is
            return items

    ranks = {c: get_rank(items, c) for c in columns}
    if is_dict(items):
        new_items = {}
        for key, item in items.items():
            new = deepcopy(item)
            new.update({f'@{c}': ranks[c][item[c]] for c, rank in ranks.items()})
            new_items[key] = new

        return new_items
    else:
        new_items = []
        for item in items:
            new = deepcopy(item)
            new.update({f'@{c}': ranks[c][item[c]] for c, rank in ranks.items()})
            new_items.append(new)

        return type(items)(new_items)


def get_rank(items, column):
    unique = distinct(get_column(items, column))
    unique = {u: i for i, u in enumerate(sorted(unique))}
    return unique


# build tables =====================================================================================
def tabulate(data, header=None):
    assert is_list_or_tuple(data) and is_dict(data[0])

    header = header or list(data[0].keys())
    widths = {column: len(column) for column in header}
    for row in data:
        for column, value in row.items():
            widths[column] = max(widths[column], len(str(value)))

    widths = tuple(width + 2 for width in widths.values())

    lines = [tabulate_line(header, widths, 'center')]
    lines.append(['=' * len('|'.join(lines[0]))])
    lines.extend(tabulate_line(row.values(), widths) for row in data)
    return '\n'.join('|'.join(line) for line in lines)


def tabulate_line(values, widths, alignments=None):
    """ based on the type of the ALIGNMENTS variable:
        list: custom
        str: same for each column
        *None: numbers are right aligned, everything else is left aligned
    """

    if is_list_or_tuple(alignments):
        return tuple(align(value, width, side) for value, width, side in zip(values, widths, alignments))
    elif is_str(alignments):
        return tuple(align(value, width, alignments) for value, width in zip(values, widths))
    else:
        return tuple(align(value, width) for value, width in zip(values, widths))


# data structures ==================================================================================
def topological_sort(graph):
    sorted_nodes = []
    while graph:
        for root, nodes in deepcopy(graph).items():
            # append all childless nodes
            for node in nodes:
                if node not in sorted_nodes and not graph.get(node):
                    sorted_nodes.append(node)

            # append node if all its children are already sorted
            if set(nodes).difference(set(sorted_nodes)) == set() and root not in sorted_nodes:
                sorted_nodes.append(root)

            if root in sorted_nodes:
                del graph[root]

    return sorted_nodes


# strings ==========================================================================================
def center(value, width, fill=' '): return f' {value} '.center(width, fill)


def left(value, width, fill=' '): return f' {value} '.ljust(width, fill)


def right(value, width, fill=' '): return f' {value} '.rjust(width, fill)


def align(value, width, side=None, *, fill=' '):
    if side is None:
        side = 'right' if is_number(value) else 'left'

    if side == 'center': return center(value, width, fill)
    elif side == 'left': return left(value, width, fill)
    elif side == 'right': return right(value, width, fill)
    else:
        raise ValueError(f'side = {side}')


def minimise_whitespace(text):
    return ' '.join(text.split())


# other =============================================================================================
def depth_map(f, x, condition=is_primitive):
    """ Not very well tested PoC
    - Applies f() to all primitive elements in x
    - You can use any combination of set, tuple, list, dict or their derivatives to any arbitrary depth (respecting python's 1000 recursive calls limit)
        - e.g. dict of lists of tuples of sets
    """

    if condition(x):
        x = f(x)

    if isinstance(x, dict):
        x = {k: depth_map(f, v, condition) for k, v in x.items()}
    elif is_iterable(x) and not isinstance(x, str):
        x = type(x)(depth_map(f, xi, condition) for xi in x)

    return x


def polarize_confidences(predictions):
    """calculate the weight based on a kind of sum of probability density distribution
    if two predictions are very close together they increase each other's confidence
    e.g. if you predict y = 5 with the highest likelihood, but the predictions for y = 2 and y = 3 are almost as likely
    2 and 3 each get a greater score than 5"""
    updated = defaultdict(int)
    for (a, w_1), (b, w_2) in itertools.product(predictions, predictions):
        updated[a] += w_2 / (abs(a - b) or 1)  # `or 1` avoids DivisionByZeroError

    return list(updated.items())


def polarize_confidences_and_pick_the_most_confident_one(predictions):
    """sort by confidence and return the value associated with it"""
    if predictions:
        return max(polarize_confidences(predictions), key=lambda xi: xi[1])[0]
    else:
        return None


def normalize_sum_to_1(values):
    if values and len(values[0]) == 2:
        # [(pred_1, probability_1), (pred_2, probability_2), ...]
        total = sum(b for a, b in values)
        return type(values)((a, b / total) for a, b in values)
    else:
        # [probability_1, probability_2, ...]
        total = sum(values)
        return type(values)(a / total for a in values)


def difference(A, B):
    """Removes duplicates"""
    return type(A)(set(A).difference(B))


def intersection(A, B):
    """Removes duplicates"""
    if len(B) < len(A):  # avoid unnecessary computation
        return intersection(B, A)
    else:
        return type(A)(set(A).intersection(B))


def sitems(d):
    return sorted(d.items())


def shuffle(data, seed=None):
    from utils.datetime_utils import get_datetime
    if seed is None:
        seed = int(get_datetime().strftime('%Y%m%d'))

    if is_pandas_df(data):
        return data.sample(frac=1, random_state=seed).reset_index(drop=True)
    else:
        raise NotImplementedError


def skeys(d):
    return sorted(d.keys())


def distinct(x):
    try:
        tmp_x = sorted(x)
    except Exception:
        tmp_x = sorted(list(x))

    new_x = []
    for xi in tmp_x:
        if xi not in new_x:
            new_x.append(xi)

    return type(x)(new_x)


def get_column(X, i):
    """ TODO: implement giving a list of columns instead of just one
    - it's like getting the column i of a numpy matrix like this A[:, i], but you are using a dict, list or tuple instead
    e.g.:
    A = np.asarray([[1,2,3],[4,5,6],[7,8,9]])
    A[:, 0] # => [1, 4, 7]
    A[:, 1] # => [2, 5, 8]
    A[:, 2] # => [3, 6, 9]
    """
    if len(X):
        if is_dict(X):
            return [item[i] for item in X.values()]
        else:
            return type(X)(item[i] for item in X)
    else:
        return X


def get_columns(data, cols, with_keys=True):
    if is_primitive(cols):
        cols = [cols]

    if is_dict(data):
        if with_keys:
            return {k: data[k] for k in cols}
        else:
            return tuple(data[k] for k in cols)
    else:
        return type(data)(get_columns(d, cols, with_keys=with_keys) for d in data)


def diff(A, B):
    assert type(A) == type(B)

    if is_dict(A):
        A = A.items()
        B = B.items()

    return {
        'left': [a for a in A if a not in B],
        'center': [a for a in A if a in B],
        'right': [b for b in B if b not in A]
    }


def count(X, key, value):
    """
    Count how many dicts/tuples/lists/strings in the list have `value` at the specified key/index
        - e.g. count([{'a': 1, 'b': 2}, {'b': 1}, {'a': 3}, {'a': 1}], 'a', 1) # => 2
        - e.g. count([(0,0), (0,5), (5, 0)], 1, 5) # => 1

    Instead, if you want to count the number of `value` occurences in a list, use my_list.count(value)
        - e.g. [{'a': 1}, {'b': 2}, {'a': 3}, {'a': 1}].count({'a': 1}) # => 2
    """
    if X and isinstance(X[0], dict):
        return sum(1 if xi.get(key) == value else 0 for xi in X)
    else:
        return sum(1 if xi[key] == value else 0 for xi in X)


def xor(a, b):
    return bool(a) ^ bool(b)


def all_equal(x, key=None):
    if key:
        return len({xi[key] for xi in x}) == 1
    else:
        return len(set(x)) == 1


def all_equal_length(x):
    return all_equal(map(len, x))


def all_same_keys(data):
    if is_iterable(data):
        if data:
            expected = data[0].keys()
            return all((row.keys() == expected) for row in data[1:])
        else:
            return True
    else:
        raise TypeError(f'Unexpected type({type(data)}')


def approx_equal(A, B, absolute=1e-6, relative=1e-6, enforce_same_type=False):
    if enforce_same_type and type(A) != type(B) and not is_primitive(A):
        # I use `not is_primitive(A)` to enforce the same type only for data structures
        return False

    try:
        is_approx_equal = math.isclose(A, B, rel_tol=relative, abs_tol=absolute)
    except TypeError:
        is_approx_equal = False

    if is_approx_equal:
        return True
    elif is_primitive(A) and is_primitive(B):
        return A == B
    elif xor(is_primitive(A), is_primitive(B)):
        # one is primitive and the other is iterable
        return False
    elif len(A) != len(B):
        return False
    elif is_set(A) and is_set(B):
        A, B = sorted(A), sorted(B)
        if len(A) != len(B):
            return False

        for a, b in zip(A, B):
            if not approx_equal(a, b, absolute, relative):
                return False

        return True
    elif is_dict(A) and is_dict(B):
        for k in A.keys():
            if not approx_equal(A[k], B[k], absolute, relative):
                return False

        return True
    elif is_list_or_tuple(A) and is_list_or_tuple(B):
        for a, b in zip(A, B):
            if not approx_equal(a, b, absolute, relative):
                return False

        return True
    else:
        return False


def summary(X, n=10, shuffle=True):
    if isinstance(X, dict):
        X = list(X.items())
    elif type(X).__name__.startswith('dict_'):
        X = list(X)

    if shuffle:
        X = random.choices(X, k=n)

    pprint(X[:n])


def remove_columns(items, columns):
    """TODO: return the removed columns too"""
    if is_primitive(columns):
        columns = [columns]

    if is_pandas_df(items):
        return items.drop(columns, axis=1)
    elif is_dict(items):
        return {k: v for k, v in items.items() if k not in columns}
    elif is_dict(items[0]):
        return type(items)({k: v for k, v in item.items() if k not in columns} for item in items)
    else:
        return type(items)(type(items[0])(v for i, v in enumerate(item) if i not in columns) for item in items)


def iround(x, decimals=6):
    if type(x).__name__ in ('generator', 'map'):
        x = tuple(x)

    if len(x) != 0 and is_iterable(x[0]):
        return type(x[0])(map(lambda xi: iround(xi, decimals), x))

    return type(x)(map(lambda xi: round(xi, decimals), x))


def izip(*x):
    return type(x[0])(zip(*x))


def ifilter(f, x):
    return type(x)(filter(f, x))


def imap(f, x, filter_f=None):
    if filter_f:
        return type(x)(map(f, ifilter(filter_f, x)))
    else:
        return type(x)(map(f, x))


def extend(expand_fn, source):
    accumulator = []
    for item in source:
        accumulator.extend(expand_fn(item))

    return accumulator


def dif(x):
    a, b = x
    if (a is None and b is not None) or (a is not None and b is None):
        return math.inf
    elif (a is None) and (b is None):
        return 0
    else:
        return a - b


def iadd(x):
    return imap(lambda xi: xi[0] + xi[1], izip(*x))


def idif(x):
    return imap(dif, izip(*x))


def iabs(x):
    return imap(abs, x)


def iadif(x):
    return iabs(idif(x))


def ifloat(x):
    return imap(float, x)


def tsorted(x):
    return tuple(sorted(x))


def iround_or_None(x, decimals=6):
    if type(x).__name__ in ('generator', 'map'):
        x = tuple(x)

    first = head(x, 1)
    if x is None:
        return None
    elif is_primitive(x):
        return round(x, decimals)
    elif is_iterable(x) and len(x) != 0 and is_iterable(first):
        return type(x)(map(lambda xi: iround_or_None(xi, decimals), x))

    return type(x)(map(lambda xi: None if xi is None else round(xi, decimals), x))


def order_by(X, columns):
    if is_primitive(columns):
        columns = [columns]

    if len(columns) >= 2:
        """Possible solutions:
        1. create a neg() class that inverts __lt__ with __gt__ and stuffs
        2. use get_rank()
        3. group them up by the first column, sort, group again
        """
        raise NotImplementedError('Currently sorting by multiple columns does not work correctly. Previous sorts get messed up after each sort')

    new = deepcopy(X)
    for c in columns[::-1]:
        if is_number(c) and c < 0:
            reverse = True
            c = abs(c)
        elif is_str(c) and c.startswith('-'):
            reverse = True
            c = c[1:]
        else:
            reverse = False

        new = sorted(new, key=lambda xi: xi[c], reverse=reverse)

    return new


def dict_product(**kwargs):
    keys = kwargs.keys()
    values = kwargs.values()
    for combo in itertools.product(*values):
        yield dict(zip(keys, combo))


def head(X, n=1):
    if not X:
        return X
    elif is_dict(X):
        keys = iter(X.keys())
        picked = {}
        for _ in range(n):
            k = next(keys)
            picked[k] = X[k]

        return picked
    else:
        iterator = iter(X)
        if n == 1:
            return next(iterator)
        else:
            return type(X)(next(iterator) for _ in range(n))


def index_by(data, keys):
    if is_callable(keys):
        indexer = keys
    elif is_primitive(keys):
        def indexer(x): return x.get(keys)
    elif is_iterable(keys):
        if is_empty(keys):
            raise ValueError(f'\'keys\' = {repr(keys)}')
        elif len(keys) == 1:
            def indexer(x): return x.get(keys[0])
        else:
            def indexer(x): return tuple(x.get(k) for k in keys)
    else:
        raise NotImplementedError

    return {indexer(row): row for row in data}


def inner_join(A, B, on):
    tmp_A = index_by(A, on)
    tmp_B = index_by(B, on)
    result = []
    for k in tmp_A.keys():
        if k in tmp_B.keys():
            assert not has_conflicting_keys(tmp_A[k], tmp_B[k])
            result.append({**tmp_A[k], **tmp_B[k]})

    return result


def left_join(A, B, on):
    tmp_A = index_by(A, on)
    tmp_B = index_by(B, on)
    empty_B_row = {k: None for k in B[0].keys()}

    for k, row_A in tmp_A.items():
        row_B = tmp_B.get(k, empty_B_row)
        row_B = {k: v for k, v in row_B.items() if k not in on}
        assert not has_conflicting_keys(row_A, row_B)
        tmp_A[k].update(row_B)

    return list(tmp_A.values())


def full_join(A, B, on):
    tmp_A = index_by(A, on)
    tmp_B = index_by(B, on)
    columns = set(get_keys(A[0]) + get_keys(B[0]))

    result = []
    for join_key in set(get_keys(tmp_A) + get_keys(tmp_B)):
        row_A = tmp_A.get(join_key, {})
        row_B = tmp_B.get(join_key, {})
        assert not has_conflicting_keys(row_A, row_B)
        result.append({k: row_A.get(k, row_B.get(k)) for k in columns})

    return result


def self_join(): raise NotImplementedError


def cartesian_join(): raise NotImplementedError


def xor_join(): raise NotImplementedError  # full_join() - inner_join()


def get_keys(X):
    if is_dict(X):
        return sorted(X.keys())
    else:
        keys = set()
        for row in X:
            keys.update(row.keys())

        return sorted(keys)


def has_conflicting_keys(A, B, keys_to_ignore=None):
    if keys_to_ignore is None:
        keys_to_ignore = []
    elif is_primitive(keys_to_ignore):
        keys_to_ignore = [keys_to_ignore]

    return any((k not in keys_to_ignore and A[k] != B[k]) for k in get_shared_keys(A, B))


def get_shared_keys(A, B):
    return set(A.keys()).intersection(B.keys())


def set_seed(seed):
    # do local imports to avoid dependencies
    try:
        import os
        os.environ["PL_GLOBAL_SEED"] = str(seed)
    except Exception as e:
        print(f'[ERROR] os.environ["PL_GLOBAL_SEED"] = str(seed) => {e}')

    try:
        import random
        random.seed(seed)
    except Exception as e:
        print(f'[ERROR] random.seed(seed) => {e}')

    try:
        import numpy as np
        np.random.seed(seed)
    except Exception as e:
        print(f'[ERROR] np.random.seed(seed) => {e}')

    try:
        import torch
        torch.manual_seed(seed)
    except Exception as e:
        print(f'[ERROR] torch.manual_seed(seed) => {e}')

    try:
        import torch
        torch.cuda.manual_seed_all(seed)
    except Exception as e:
        print(f'[ERROR] torch.cuda.manual_seed_all(seed) => {e}')


def dict_zip(*dicts, as_generator=False):
    """if you need dict_zip_longest, use outer_join_dicts"""
    if as_generator:
        if not dicts:
            return

        n = len(dicts[0])
        if any(len(d) != n for d in dicts):
            raise ValueError('arguments must have the same length')

        for key, first_val in dicts[0].items():
            yield (key, first_val, *(other[key] for other in dicts[1:]))
    else:
        return {k: tuple(values) for k, *values in dict_zip(*dicts, as_generator=True)}


def inner_join_dicts(*dicts, as_generator=False):
    if as_generator:
        if not dicts:
            return

        keys = set(dicts[0]).intersection(*dicts[1:])
        for k in keys:
            yield (k, *(d[k] for d in dicts))
    else:
        return {k: tuple(values) for k, *values in inner_join_dicts(*dicts, as_generator=True)}


def outer_join_dicts(*dicts, default=None, as_generator=False):
    """acts as dict_zip_longest"""
    if as_generator:
        if not dicts:
            return

        keys = set(dicts[0]).union(*dicts[1:])
        for k in keys:
            yield (k, *(d.get(k, default) for d in dicts))
    else:
        return {k: tuple(values) for k, *values in outer_join_dicts(*dicts, default=default, as_generator=True)}


def fix_multiline_indentation(multiline_string):
    # remove whitespace around the string and split by newline
    multiline_string = multiline_string.strip().split('\n')

    # get indentation of each line
    indents = [re.search(r'^\s+', row) for row in multiline_string]
    indents = [item.group() for item in filter(None, indents)]
    indents = [item for item in indents if item != '']

    # get shortest indentation
    shortest_indent = min(indents) if indents else ''

    # shift left by the length of shortest_indent
    multiline_string = [re.sub('^' + shortest_indent, '', row) for row in multiline_string]

    # convert back to multiline
    multiline_string = '\n'.join(multiline_string).replace('\t', ' ' * 4)

    return multiline_string


def remove_done(todo, done):
    return type(todo)(item for item in todo if item not in done)


def first(iterable, default=None, key=bool):
    for e in iterable:
        if key(e):
            return e

    return default


def contains_any_of(haystack, needles):
    return any((item in haystack) for item in needles)


def contains_all_of(haystack, needles):
    return all((item in haystack) for item in needles)


def is_divisible_by(number, divisor):
    return number % divisor == 0


def flatten_counter(x):
    flattened = []
    for k, v in x.items():
        flattened.extend([k] * v)

    return flattened


def running_window_lead_or_lag(iterable, n=2, wrap=False, f=None, direction=None):
    """
    :param f: window transform function
    """
    if wrap:
        result = zip(*(iter(iterable[i:] + iterable[:i]) for i in range(n)))
    else:
        result = zip(*(iter(iterable[i:]) for i in range(n)))

    if f:
        result = (f(window) for window in result)

    if wrap:
        return result
    elif direction == 'leading':
        return itertools.chain([None] * (n-1), result)
    elif direction == 'lagging':
        return itertools.chain(result, [None] * (n-1))


def running_window(iterable, n=2, wrap=False):
    if not (isinstance(n, int) and n >= 2):
        raise ValueError(f'<n> expects an int >= 2. Got {n!r}')
    elif not isinstance(wrap, bool):
        raise TypeError(f'<wrap> expects True or False. Got {wrap!r}')

    iterable = iter(iterable)

    try:
        prev = [next(iterable) for _ in range(n-1)]
    except StopIteration:
        return iter([])

    if wrap:
        first = copy.deepcopy(prev)
        iterable = itertools.chain(iterable, first)

    for current in iterable:
        yield (*prev, current)
        del prev[0]
        prev.append(current)


def sort_keys(d, key=None, reverse=False):
    return dict(sorted(d.items(), key=key, reverse=reverse))


def merge_overlapping_ranges(ranges):
    ranges = sorted(ranges)

    changed = True
    while changed and len(ranges) > 1:
        new_ranges = []
        changed = False
        last_iteration = len(ranges) - 2

        for i, ((start_1, end_1), (start_2, end_2)) in enumerate(running_window_lead_or_lag(ranges)):
            if end_1 >= start_2:
                new_ranges.append((start_1, end_2))
                new_ranges.extend(ranges[i+2:])
                changed = True
                break
            elif i == last_iteration:
                new_ranges.append((start_1, end_1))
                new_ranges.append((start_2, end_2))
            else:
                new_ranges.append((start_1, end_1))

        ranges = sorted(new_ranges)

    return ranges


def as_list_of_dicts(data, ensure_equal_lengths=True):
    if is_list(data) and all(is_dict(row) for row in data):
        if data and ensure_equal_lengths:
            assert all_same_keys(data)

        return data
    elif is_dict(data) and all(is_list(row) for row in data.values()):
        if ensure_equal_lengths:
            assert all_equal_length(data.values())

        keys = list(data.keys())
        n = len(data[keys[0]])
        return [{k: data[k][i] for k in keys} for i in range(n)]
    else:
        raise ValueError('Unexpected format')


def as_dict_of_lists(data, ensure_equal_lengths=True):
    if is_dict(data) and all(is_list(row) for row in data.values()):
        if data and ensure_equal_lengths:
            assert all_equal_length(data.values())

        return data
    elif is_list(data) and all(is_dict(row) for row in data):
        if ensure_equal_lengths:
            assert all_same_keys(data)

        result = defaultdict(list)
        for row in data:
            for k,v in row.items():
                result[k].append(v)

        return result
    else:
        raise ValueError('Unexpected format')


def xyXY_from_xywh(xywh):
    x, y, w, h = xywh
    return (x, y, x+w, y+h)


def xy_XY_from_xywh(xywh):
    xyXY = xyXY_from_xywh(xywh)
    return xy_XY_from_xyXY(xyXY)


def xy_XY_from_xyXY(xyXY):
    x, y, X, Y = xyXY
    return ((x, y), (X, Y))


def xywh_from_xyXY(xyXY):
    x, y, X, Y = xyXY
    return (x, y, X-x, Y-y)


def xywh_from_xy_XY(xy_XY):
    (x, y), (X, Y) = xy_XY
    return (x, y, X-x, Y-y)
