import itertools

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix

from utils.data import is_pandas_df
from utils.decorators import vectorize
from utils.logging_utils import warn


def plt_clear():
    plt.clf()
    plt.cla()
    plt.close()


def plt_show():
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()


def plt_timeseries(data, date_column, value_column=None):
    """
    - if data is a list of tuples, use the index (e.g. 0) else use the key (e.g. 'date')
    - if data contains 2 columns, you can omit value_column, otherwise give it the key/index you want to plot
    """
    if not is_pandas_df(data):
        data = pd.DataFrame(data)

    if value_column is None:
        value_column = [c for c in data.columns if c != date_column][0]

    data = data.sort_values(date_column)
    data[date_column] = data[date_column].astype("datetime64")

    plt.plot_date(data[date_column], data[value_column], linestyle='solid')
    plt.gcf().autofmt_xdate()
    plt.tight_layout()
    plt_show()


def plt_hist(data, n=None, fn=None):
    plt.rcParams.update({
        "ytick.color": "w",
        "xtick.color": "w",
        "axes.labelcolor": "w",
        "axes.edgecolor": "w"
    })

    # convert to list of tuples
    if isinstance(data, dict): data = data.items()

    # sort by frequency
    data = sorted(data, key=lambda xi: xi[1], reverse=True)

    if n: data = data[:n]

    plt.bar(*zip(*data))
    plt.xticks(rotation=45)
    if fn:
        plt.savefig(fn)

    plt_show()


def plt_scatter(points):
    plt_clear()
    for p in points:
        plt.scatter(p[0], p[1])
    plt.gca().invert_yaxis()
    plt.show()
    plt_clear()


def plot(y, X=np.linspace(-10, 10, 100), *,
    styles=('-o', '.', '-', ':', '--', '-.', 'P'),
    colors=('red', 'green', 'orange', 'cyan', 'magenta', 'yellow', 'blue')
):
    """
    :param y
        f               -> function / lambda / callable object
        [f1, ...]       -> list of functions
        [y1, ...]       -> list of values
        {x1: y1, ...}   -> dict of (x, y) pairs. Ignoring the `X` arg
        [(x1, y1), ...] -> iterable of (x, y) tuples. Ignoring the `X` arg

    :param X
        default -> 100 equally spaced points in the [-10, 10] range

    :param colors/styles
        plot([<= 7 functions], colors=['red']) -> use only red with as many styles as needed
        plot([<= 7 functions], styles=['*']) -> use only '*' with as many colors as needed
        plot([ > 7 functions], ...) -> use as many colors and styles as needed, ignoring the specified options
    """
    def plot_(y, X, style, color):
        if isinstance(y, dict):
            X, y = zip(*y.items())
        elif hasattr(y, '__iter__'):
            if hasattr(y[0], '__iter__'):
                X, y = zip(*y)
            elif len(X) < len(y):
                warn('Filling X with None to make the lengths match')
                X, y = zip(*itertools.zip_longest(X, y))
            elif len(y) < len(X):
                warn('Filling y with None to make the lengths match')
                X, y = zip(*itertools.zip_longest(X, y))
            else:
                y = y  # I'm just being explicit
        else:
            try:
                y = y(X)
            except Exception:
                y = vectorize(y)
                y = y(X)

        plt.plot(X, y, style, color=color)

    def increase_options(n):
        all_styles = ('-o', '.', '-', ':', '--', '-.', 'P'),
        all_colors = ('red', 'green', 'orange', 'cyan', 'magenta', 'yellow', 'blue')

        n_combos = len(all_styles) * len(all_colors)
        assert n <= n_combos, f'{n} functions and only {n_combos} (style, color) combos'

        return all_styles, all_colors

    if not hasattr(y, '__iter__'): y = [y]
    if isinstance(colors, str): colors = [colors]
    if isinstance(styles, str): styles = [styles]

    if (len(styles) * len(colors)) < len(y):
        styles, colors = increase_options(len(y))

    for yi, (s, c) in zip(y, itertools.product(styles, colors)):
        plot_(yi, X, s, c)

    plt_show()


def show_confusion_matrix(expected, predicted):
    n = max(*expected, *predicted)
    r = range(n+1)
    cm = confusion_matrix(expected, predicted)
    max_count = cm.max()

    fig, ax = plt.subplots(figsize=(2 + n//2, 2 + n//2))
    ax.imshow(cm)
    ax.grid(False)
    ax.set_xlabel('Predicted')
    ax.set_ylabel('Expected')
    ax.xaxis.set(ticks=r)
    ax.yaxis.set(ticks=r)
    ax.set_ylim(n + 0.5, -0.5)

    for i, j in itertools.product(r, r):
        if cm[i, j] < 0.5 * max_count:
            value_color = 'white'
        else:
            value_color = 'black'

        ax.text(j, i, cm[i, j], ha='center', va='center', color=value_color)

    plt_show()


def plot_train_test_val(train, test, val, model=None):
    """
    - model is not None -> plot the linear/logistic regressor
    - https://files.realpython.com/media/fig-2.850628602c7e.png
    """
    raise NotImplementedError


def plot_predictions(expected, predicted, model=None):
    """
    - model is not None -> plot the linear regressor or the logistic
    - https://files.realpython.com/media/log-reg-3.b1634d335c4f.png
    - https://files.realpython.com/media/log-reg-2.e88a21607ba3.png
    """

    raise NotImplementedError
