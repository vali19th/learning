import itertools
import math
from collections import Counter
from pprint import pprint

import numpy as np

from utils.data import is_iterable, is_line, is_line_with_width, is_list_or_tuple, is_polygon


def get_stats(ground_truth, predictions):
    assert len(ground_truth) == len(predictions)

    tp = tn = fp = fn = 0
    for t, p in zip(ground_truth, predictions):
        if   (t, p) == (0, 0): tn += 1
        elif (t, p) == (0, 1): fp += 1
        elif (t, p) == (1, 0): fn += 1
        elif (t, p) == (1, 1): tp += 1

    return {
        'FP': fp, 'FN': fn, 'TP': tp, 'TN': tn,
        'accuracy': (tp + tn) / len(ground_truth),
        'sensitivity': tp / (tp + fn),
        'specificity': tn / (fp + tn),
        'PPV': tp / (tp + fp),
        'NPV': tn / (tn + fn),
    }


def aggregate(x):
    return {
        'min': min(x),
        'max': max(x),
        'sum': sum(x),

        'mean': mean(x),
        'geometric_mean': geometric_mean(x),
        'harmonic_mean': harmonic_mean(x),
        'mode': mode(x),
        'median': median(x),
    }


def min_max(x):
    return min(x), max(x)


def mean(x):
    if isinstance(x, dict):
        return sum(k * v for k,v in x.items()) / (sum(x.values()) or 1)
    else:
        return sum(x) / (len(x) or 1)


def geometric_mean(x): return math.prod(x) ** (1/len(x))
def harmonic_mean(x): return len(x) / sum(1/xi for xi in x)


def L1(p1, p2):
    assert len(p1) == len(p2)
    return sum(abs(a - b) for a, b in zip(p1, p2))


def L2(p1, p2):
    assert len(p1) == len(p2)
    return math.sqrt(sum((a - b) ** 2 for a, b in zip(p1, p2)))


def intercept_with_axes(line):
    A, B, C = line_general_form(line)
    x = C/A if A else None
    y = C/B if B else None

    return (x, y)


def get_rotation_matrix(angle):
    """angle: any number in the range [0, 360]"""
    angle = np.radians(angle)
    c, s = np.cos(angle), np.sin(angle)
    return np.array([(c, -s), (s,  c)])


def get_polygon_centroid(polygon):
    return np.sum(polygon, axis=0) / len(polygon)


def rotate_polygon_with_rotation_matrix(polygon, angle, C=None):
    """Args:
        polygon: [..., (xi, yi), ...]
        angle: in degrees
        C: the (x, y) point around which to rotate or None to automatically compute the polygon's centroid
    """
    P = np.array(polygon)
    R = get_rotation_matrix(angle)

    if C is None:
        C = get_polygon_centroid(polygon)

    C = np.array([C for _ in range(len(polygon))])

    return (P - C).dot(R.T) + C


def line_general_form(line):
    (x1,y1), (x2,y2) = line
    A = (y1 - y2)
    B = (x2 - x1)
    C = (x1*y2 - x2*y1)
    return A, B, -C


def cartesian(r, t, offset=(0,0)):
    t = math.radians(t)
    return (offset[0] + r * math.cos(t), offset[1] + r * math.sin(t))


def cartesian2(r, t, offset=(0,0)):
    t = math.radians(t)
    return offset, (offset[0] + r * math.cos(t), offset[1] + r * math.sin(t))


def polar(x, y=None):
    if y is None and is_line(x):
        x, y = x

    if is_list_or_tuple(x) and is_list_or_tuple(y):
        return polar(y[0] - x[0], y[1] - x[1])  # x and y are actually point_1 and point_2, not the x and y coords
    else:
        # I've used "% 360" twice because it acts weirdly on negative very small numbers (e.g. -3.04e-15)
        return math.hypot(x, y), math.degrees(math.atan2(y, x)) % 360 % 360


def is_slanted(data):
    if is_line(data):
        return get_angle(*data) % 90 != 0
    elif is_line_with_width(data):
        line, width = data
        return is_slanted(line)
    elif is_polygon(data):
        line, w = poly_to_line_with_width(data, decimals=20)
        return is_slanted(line)


def colinear_points(pts, decimals=6):
    return len(set(round(get_angle(p1, p2) % 180, decimals) for p1, p2 in itertools.combinations(pts, 2))) == 1


def mode(x):
    c = Counter(x)
    max_n = c.most_common(1)[0][1]
    most_common = [k for k, n in c.items() if n == max_n]
    return sum(most_common) / len(most_common)


def median(x):
    if len(x) % 2:
        return sorted(x)[int(len(x)/2)]
    else:
        mid = int(len(x)/2)
        return sum(sorted(x)[mid-1:mid+1])/2


def MAE(data, cycle=None):
    '''Mean Absolute Error
    if your values are cyclical and continuous (e.g. degrees from 0 to 89) and the ends are actually close to each other,
        you can set `cycle` equal to the max value + 1 so you can have a more accurate loss (in the degrees case, use 90)
    '''

    n = (len(data) or 1)
    if cycle:
        return sum(min(abs(a - b), abs(a - cycle + b)) for a, b in data) / n
    else:
        return sum(abs(a - b) for a, b in data) / n


def MSE(data, cycle=None):
    '''Mean Squared Error
    if your values are cyclical and continuous (e.g. degrees from 0 to 89) and the ends are actually close to each other,
        you can set `cycle` equal to the max value + 1 so you can have a more accurate loss (in the degrees case, use 90)
    '''

    n = (len(data) or 1)
    if cycle:
        return sum(min((a - b)**2, (a - cycle + b)**2) for a, b in data) / n
    else:
        return sum((a - b)**2 for a, b in data) / n


def RMSE(data, cycle=None):
    '''Root Mean Squared Error
    if your values are cyclical and continuous (e.g. degrees from 0 to 89) and the ends are actually close to each other,
        you can set `cycle` equal to the max value + 1 so you can have a more accurate loss (in the degrees case, use 90)
    '''
    return math.sqrt(MSE(data, cycle=cycle))


def get_angle(A, B):
    return polar(A, B)[1]


def poly_to_line_with_width(poly, decimals=6):
    side_1, side_2 = sorted(itertools.combinations(poly, 2), key=lambda c: L2(*c))[:2]  # parallel lines
    line = vavg2(side_1), vavg2(side_2)
    width = round(L2(*side_1) + 1, decimals)

    return line, width


def get_rectangle_from_line_with_width(line, length_percentage=1):
    # expecting the line in format (((x1, y1), (x2, y2)), width)
    return get_four_rectangle_points_from_line_with_width(*line[0][0], *line[0][1], line[1], length_percentage)


def get_four_rectangle_points_from_line_with_width(x1, y1, x2, y2, width, length_percentage=1):
    width = float(width)-1
    perpendicular = (y2 - y1, x1 - x2)
    length = math.sqrt(perpendicular[0]**2 + perpendicular[1]**2) * length_percentage
    norm_perp = (perpendicular[0] / length, perpendicular[1] / length)
    p1 = (x1 - norm_perp[0] * width / 2, y1 - norm_perp[1] * width / 2)
    p2 = (x2 - norm_perp[0] * width / 2, y2 - norm_perp[1] * width / 2)
    p3 = (x2 + norm_perp[0] * width / 2, y2 + norm_perp[1] * width / 2)
    p4 = (x1 + norm_perp[0] * width / 2, y1 + norm_perp[1] * width / 2)

    return p1, p2, p3, p4


def vavg2(v, weights=None):
    assert weights is None or len(v) == len(weights)
    n = len(v)
    if n == 0:
        return None
    else:
        if weights is None:
            weights = [1]*n

        sum_w = sum(weights)
        if is_iterable(v[0]):
            final = []
            for i in range(len(v[0])):
                sum_vi = sum(val[i] * w for val, w in zip(v, weights))
                final.append(sum_vi / sum_w)

            return tuple(final)
        else:
            sum_vi = sum(val * w for val, w in zip(v, weights))
            return sum_vi / sum_w


def get_bbox_area(bbox):
    x, y, X, Y = bbox
    return (X - x) * (Y - y)
