import re

# TODO: use enum.Enum


class RX:
    """The patterns are not strict enough to avoid all the possible false positives"""

    # unsigned
    UFLOAT = r'(?:[0-9]+\.[0-9]+)'
    UINT = r'(?:[0-9]+)'
    UNUM = f'(?:{UFLOAT}|{UINT})'

    # MAYBE signed with '+' or '-'
    FLOAT = f'(?:[-+]?{UFLOAT})'
    INT = f'(?:[-+]?{UINT})'
    NUM = f'(?:{FLOAT}|{INT})'

    # signed with '+' or '-'
    SFLOAT = f'(?:[-+]{UFLOAT})'
    SINT = f'(?:[-+]{UINT})'
    SNUM = f'(?:{SFLOAT}|{SINT})'

    COORDS = f'(?:({NUM})\s*,\s*({NUM}))'

    SVG_PATH_A = f'(?:([aA])\s*({NUM})\s*,?\s*({NUM})\s*({NUM})\s*({NUM})\s*,?\s*({NUM})\s*({COORDS}))'
    SVG_PATH_C = f'(?:([cC])\s*({COORDS})\s*({COORDS})\s*({COORDS}))'
    SVG_PATH_H = f'(?:([hH])\s*({NUM}))'
    SVG_PATH_L = f'(?:([lL])\s*({COORDS}))'
    SVG_PATH_M = f'(?:([mM])\s*({COORDS}))'
    SVG_PATH_Q = f'(?:([qQ])\s*({COORDS})\s*({COORDS}))'
    SVG_PATH_S = f'(?:([sS])\s*({COORDS})\s*({COORDS}))'
    SVG_PATH_T = f'(?:([tT])(\s*{COORDS})+)'
    SVG_PATH_V = f'(?:([vV])\s*({NUM}))'

    SVG_PATH_ANY = f'(?:{SVG_PATH_A}|{SVG_PATH_C}|{SVG_PATH_H}|{SVG_PATH_L}|{SVG_PATH_M}|{SVG_PATH_Q}|{SVG_PATH_S}|{SVG_PATH_T}|{SVG_PATH_V})'

    HEXCHAR = r'(?:[0-9a-fA-F])'
    HEX_SHORT = f'(?:#{HEXCHAR}{{3}})'  # red = "#f00"
    HEX_LONG = f'(?:#{HEXCHAR}{{6}})'  # red = "#ff0000"
    HEX = f'(?:{HEX_LONG}|{HEX_SHORT})'

    YEAR = r'(?:[0-9]{4})'
    MONTH = r'(?:0[1-9]|1[0-2])'
    DAY = r'(?:[0-2][0-9]|3[01])'
    DATE = f'(?:{YEAR}-{MONTH}-{DAY})'
    DATE_REVERSED = f'(?:{DAY}-{MONTH}-{YEAR})'

    HOUR = r'(?:[01][0-9]|2[0-3])'
    MINUTE = r'(?:[0-5][0-9])'
    SECOND = r'(?:[0-5][0-9])'
    SECOND_AND_MS = r'(?:[0-5][0-9]\.[0-9]+)'

    TIME = f'(?:{HOUR}:{MINUTE}(:{SECOND})?)'
    TIME_WITH_S = f'(?:{HOUR}:{MINUTE}:{SECOND})'
    TIME_WITH_S_AND_MS = f'(?:{HOUR}:{MINUTE}:{SECOND_AND_MS})'
    TIME_ANY_COMBO = f'(?:{TIME_WITH_S_AND_MS}|{TIME_WITH_S}|{TIME})'

    DATETIME = f'(?:{DATE} {TIME})'
    DATETIME_WITH_S = f'(?:{DATE} {TIME_WITH_S})'
    DATETIME_WITH_S_AND_MS = f'(?:{DATE} {TIME_WITH_S_AND_MS})'
    DATETIME_ISO = f'(?:{DATE}T{TIME_WITH_S_AND_MS}Z?)'
    DATETIME_ANY_COMBO = f'(?:{DATETIME_ISO}|{DATETIME_WITH_S_AND_MS}|{DATETIME_WITH_S}|{DATETIME})'
    DATETIME_OR_DATE_OR_TIME = f'(?:{DATETIME_ANY_COMBO}|{DATE}|{TIME_ANY_COMBO})'

    PATH_EXT_IMAGE = r'\.(?:GIF|gif|JPEG|jpeg|JPG|jpg|PNG|png)$'

    """ TODO:
    rgb(...)
    rgba(...)
    hsv(...)
    hsl(...)
    (255, 0, 0)
    (255, 0, 0, 0)
    long x lat coords
    w x h x l size
    w x h size
    w x l size
    xml tag with all attributes
        - closed/unclosed
            - if unclosed, you have to get the inner content too
                - how do I get the nested ones correctly?
                    - e.g. many <g></g> inside each-other
                    - I need a way to count them
    email
    time requires 24h or 12h formats
    date requires multiple formats (year first, year last, dash or slash or dot, etc)
    datetime_or_partial (date, time)

    unit-tests
        - make sure there are no false-negatives (more important than preventing FP)
        - prevent false-positives
    """

    @staticmethod
    def search(pattern, string, groups=None, flags=0):
        match = re.search(pattern, string, flags)
        if not match:
            return ''
        elif groups is not None:
            # Check `is not None` so it does not give false negative when `groups = 0`
            return match.group(groups)
        elif match.groups():
            return (match.group(), *match.groups())
        else:
            return match.group()
