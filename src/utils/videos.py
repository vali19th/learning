import cv2
import numpy as np
import skvideo.io
import skvideo.utils
from tqdm import tqdm

from utils.data import is_iterable, is_str
from utils.files import ls


def extract_images_from_video(input_video, output_dir, extension='png', use_prefix=True, modulo_per_frame=1, modulo_per_second=None):
	"""TODO: replace cv2 with skvideo"""
	already_extracted = ls(output_dir, as_paths=True)
	already_extracted = {p for p in already_extracted if input_video in p}

	if use_prefix:
		prefix = input_video.rpartition('/')[-1] + '_'
	else:
		prefix = ''

	if modulo_per_second:
		fps = get_video_fps(input_video)
		modulo_per_frame = int(modulo_per_second * fps)

	n = get_video_number_of_frames(input_video)
	video = open_video(input_video)
	for count in range(n):
		success, image = video.read()
		if not success:
			break

		if count % modulo_per_frame:
			continue

		path = f"{output_dir}/{prefix}{count:>06}.{extension}"
		if path not in already_extracted:
			cv2.imwrite(path, image)


def merge_images_into_video(input_dir, output_video, n, fps):
	"""TODO: replace cv2 with skvideo"""
	images = ls(input_dir)
	images = images[::n]  # keep 1/n frames

	# video fps and resolution
	height, width, layers = cv2.imread(f'{input_dir}/{images[0]}').shape
	cc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
	video = cv2.VideoWriter(output_video, cc, fps, (width, height))

	for image in tqdm(images):
		video.write(cv2.imread(f'{input_dir}/{image}'))

	cv2.destroyAllWindows()
	video.release()


def get_video_resolution(path):
	video = open_video(path)
	return video.get(cv2.CAP_PROP_FRAME_WIDTH), video.get(cv2.CAP_PROP_FRAME_HEIGHT)


def get_video_fps(path):
	video = open_video(path)
	return video.get(cv2.CAP_PROP_FPS)


def get_video_number_of_frames(path):
	video = open_video(path)
	return int(video.get(cv2.CAP_PROP_FRAME_COUNT))


def open_video(path):
	video = cv2.VideoCapture(path)
	assert video.isOpened(), f'Could not open {path!r}'

	return video


def merge_videos(videos, output_video):
	"""videos is either a list of paths or a path to the directory with the input videos"""
	if is_str(videos):
		paths = ls(videos, as_paths=True)
	elif is_iterable(videos):
		paths = videos
	else:
		raise ValueError(f'Expected `videos` of type list or str, got {type(videos).__name__!r} instead')

	out = skvideo.io.vread(paths[0])
	for path in tqdm(paths[1:]):
		video = skvideo.io.vread(path)
		out = np.concatenate([out, video])

	skvideo.io.vwrite(output_video, out)
