import subprocess


def shell(cmd, **kwargs):
    return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8').stdout.strip()


def replace_pairs(text, pairs):
    for before, after in pairs:
        text = text.replace(str(before), str(after))

    return text


def izip(*x):
    return type(x[0])(zip(*x))


def get_range_from_center_and_width(center, width=1):
    center = round(center)
    start = round(center - (width-1)/2)
    end = round(center + (width+1)/2)

    if width % 4 == 0:
        start += 1
        end += 1

    return (start, end)


def clip(value, left, right):
    if value < left:
        return left
    elif value > right:
        return right
    else:
        return value


def natural_sort(x):
    sorted_by_char = sorted(x)
    sorted_by_len_and_char = sorted(sorted_by_char, key=len)
    return sorted_by_len_and_char


def diff(A, B):
    assert type(A) == type(B)

    if isinstance(A, dict):
        A = A.items()
        B = B.items()

    return {
        'left': [a for a in A if a not in B],
        'center': [a for a in A if a in B],
        'right': [b for b in B if b not in A]
    }
