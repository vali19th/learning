import io
import os
import re
from collections import Counter, defaultdict
from io import BytesIO
import math

import cv2
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import PIL.Image as PIL_Image
from PIL import ImageFont, ImageDraw
from matplotlib.image import FigureImage

from skimage import *
from skimage import morphology
from skimage.draw import polygon_perimeter
from skimage.morphology import convex_hull_image
from skimage.io import (
    imread as sk_imread,
    imsave as sk_imsave
)
from skimage.color import *
from skimage.filters import *
from skimage.filters.thresholding import *
from skimage.measure import *
from skimage.exposure import *

from utils.data import imap, is_numpy_array, is_PIL_Image, is_str
from utils.general import *


def is_view(A):
    return A.base is not None


def split_channels(image):
    n = get_num_channels(image)
    return np.squeeze(np.split(image, n, -1), axis=-1)


def get_num_channels(A):
    return A.shape[-1] if A.ndim == 3 else 1


def get_resolution(A):
    return A.shape[:2]


def add_dimension(A):
    return np.expand_dims(A, axis=A.ndim)


def idx_of_max(array, axis=None):
    return np.unravel_index(np.argmax(array, axis=axis), array.shape)


def idx_of_min(array, axis=None):
    return np.unravel_index(np.argmin(array, axis=axis), array.shape)


def white_balance_GW(image, lightness_multiplier=1):
    """white balancing based on the Grey-World assumption"""
    image = cv2.cvtColor(image, cv2.COLOR_RGB2LAB)
    L, a, b = split_channels(image)

    lightness = (L / 255.0) * lightness_multiplier
    image[:, :, 1] = a - ((np.mean(a) - 128) * lightness)
    image[:, :, 2] = b - ((np.mean(b) - 128) * lightness)

    return cv2.cvtColor(image, cv2.COLOR_LAB2RGB)


def white_balance_white_patch_reference(image):
    closeness_matrix = 1 - color_distance(image, (255,255,255))
    idx = idx_of_max(closeness_matrix)

    brightest_pixel = image[idx]
    normalized = image / brightest_pixel
    return normalized.clip(0, 1)


def color_distance(image, color):
    """
    Returns the normalized distance of between each pixel and the color.
    Only RGB images are supported at the moment
    """
    R, G, B = color
    r, g, b = split_channels(image.astype(np.uint32))

    distance = (r-R)**2 + (g-G)**2 + (b-B)**2
    limit = (255**2 + 255**2 + 255**2)
    normalized = distance / limit
    return normalized


def remove_small_blobs(img, min_area, area_out_of_total=False):
    clean = np.zeros(img.shape[:2])
    blobs = instance_segmentation(img, True)

    if area_out_of_total:
        area_threshold = img.sum() * min_area
        blobs = sorted(blobs, key=lambda blob: blob.sum(), reverse=True)

        for blob in blobs:
            clean = ij_mask_union(clean, blob)
            if clean.sum() >= area_threshold:
                break
    else:
        for blob in blobs:
            if blob.sum() >= min_area:
                clean += blob

    return clean


def get_greatest_blob(mask):
    return max(instance_segmentation(mask, one_image_per_blob=True), key=lambda blob: blob.sum())


def instance_segmentation(img, one_image_per_blob=False):
    blobs = label(load_image(img, 'gray') > 0)
    if one_image_per_blob:
        separated_blobs = []
        for color in range(1, blobs.max() + 1):
            blob = np.zeros(img.shape[:2])
            blob[blobs == color] = 1
            separated_blobs.append(blob)

        return separated_blobs
    else:
        return blobs


def save_image(image, path):
    if type(image) == PIL_Image.Image:
        image.save(path)
    elif type(image) == np.ndarray:
        # works with skimage.io.imread(), cv2.imread(), matplotlib.image.imread(), np.asarray(PIL.Image.open())
        if image.dtype == np.float64 and np.array_equal(image, image.astype(np.uint32)) and image.max() > 1:
            image = image.astype(np.uint32)
        elif image.dtype == np.float64:
            image = img_as_ubyte(image)

        sk_imsave(path, image)
    elif type(image) == mpl.figure.Figure:
        plt.savefig(path, bbox_inches='tight', pad_inches=0)
    else:
        raise TypeError(f'save_image() not defined for this type of image: {type(image)}')


def resolution(image):
    if is_str(image):
        if os.path.exists(image):
            return imap(int, re.search('(\d+)\s*x\s*(\d+)', shell(f'file {image}')).groups())
        else:
            raise FileNotFoundError(image)
    elif is_PIL_Image(image):
        return image.size
    elif is_numpy_array(image):
        return image.shape[:2][::-1]
    else:
        raise NotImplementedError(repr(type(image)))


def crop_image(img, x, y, X, Y):
    x, y, X, Y = map(int, (x, y, X, Y))
    return load_image(img)[y:Y+1, x:X+1]


def crop_image_relative(img, left=None, top=None, right=None, bottom=None):
    img = load_image(img)
    h, w = img.shape[:2]

    left = int(left * w) if left else 0
    top = int(top * h) if top else 0
    right = int((1-right) * w) if right else w
    bottom = int((1-bottom) * h) if bottom else h

    return crop_image(img, left, top, right, bottom)


def figure_image(figure, image):
    """Adds a FirgureImage instance to a figure.

    A FigureImage is an image which is not transformed in the mpl
    stack, and so its pixels are 1-to-1 with the final figure. If the
    final figure is not of the **exact** size of the image, parts of the
    image will either be cropped, or parts of the figure will be empty.

    """
    figimg = FigureImage(figure)
    figimg.set_array(image)
    figure.images.append(figimg)
    return figimg


def load_image(img, mode=None):
    '''Loads an image or converts a numpy array to the specified mode

    Args:
        img: path to image or a numpy array
        mode: the format of the image; if None load it in its original mode

    Returns:
        image as numpy.ndarray

    Raises:
        AssertionError
        ValueError


    NOTE: some .bmp images might have the colors changed
    '''

    if isinstance(img, mpl.figure.Figure):
        io_buf = io.BytesIO()
        img.savefig(io_buf, format='raw', dpi=300)
        io_buf.seek(0)
        shape = (int(img.bbox.bounds[3]), int(img.bbox.bounds[2]), -1)
        img = np.reshape(np.frombuffer(io_buf.getvalue(), dtype=np.uint8), newshape=shape)
        io_buf.close()


    if mode == 'grey':
        mode = 'gray'

    allowed_modes = (
        None,
        'binary',
        'binary_inv',
        'binary_color',
        'binary_otsu',
        'binary_otsu_inv',
        'binary_otsu_color',
        'gray',
        'RGB',
        'RGBA',
        'HSV',
    )

    assert isinstance(img, np.ndarray) or type(img) == str, f'type(img) = {repr(type(img))}, img = {repr(img)}'
    assert mode in allowed_modes, f'mode = {repr(mode)}'

    if type(img) == str:
        img = sk_imread(img)

    dtype = str(img.dtype)
    if img.ndim == 2:
        h, w, c = *img.shape, 1
    elif img.ndim == 3 and img.shape[2] in (3, 4):
        h, w, c = img.shape
    else:
        raise ValueError(f'img.shape = {repr(img.shape)}')

    if mode == 'binary':
        img = load_image(img, mode='gray')
        img = (img > img.min()).astype(np.uint8)
    elif mode == 'binary_inv':
        img = load_image(img, mode='gray')
        img = (img == img.min()).astype(np.uint8)
    elif mode == 'binary_color':
        img = load_image(img, mode='binary')
        img = img_as_float(img)
        img = gray2rgb(img)

        bg = np.all(img==[0, 0, 0], axis=2)
        fg = np.all(img==[1, 1, 1], axis=2)
        img[bg] = [68, 1, 84]
        img[fg] = [253, 231, 36]
        img = img.astype(np.uint8)
    elif mode == 'binary_otsu':
        img = load_image(img, mode='gray')
        img = img_as_float(img)
        img = (img > threshold_otsu(img)).astype(np.uint8)

    elif mode == 'binary_otsu_inv':
        img = load_image(img, mode='gray')
        img = img_as_float(img)
        img = (img <= threshold_otsu(img)).astype(np.uint8)
    elif mode == 'binary_otsu_color':
        img = load_image(img, mode='binary_otsu')
        img = img_as_float(img)
        img = gray2rgb(img)

        bg = np.all(img==[0, 0, 0], axis=2)
        fg = np.all(img==[1, 1, 1], axis=2)
        img[bg] = [68, 1, 84]
        img[fg] = [253, 231, 36]
        img = img.astype(np.uint8)
    elif mode == 'gray':
        if c == 1:   img = img_as_ubyte(img)
        elif c == 3: img = rgb2gray(img)
        elif c == 4: img = rgb2gray(rgba2rgb(img))
        else: raise ValueError(f'mode = {repr(mode)}, c = {repr(c)}')
    elif mode == 'RGB':
        if c == 3:   pass
        elif c == 1: img = gray2rgb(img)
        elif c == 4: img = rgba2rgb(img)
        else: raise ValueError(f'mode = {repr(mode)}, c = {repr(c)}')
    elif mode == 'RGBA':
        if c == 4:                        pass
        elif c == 1:                      img = gray2rgba(img)
        elif c == 3 and 'int' in dtype:   img = np.insert(img, 3, 255, axis=2)
        elif c == 3 and 'float' in dtype: img = np.insert(img, 3, 1, axis=2)
        else: raise ValueError(f'mode = {repr(mode)}, c = {repr(c)}')
    elif mode == 'HSV':
        img = load_image(img, mode='RGB')
        img = rgb2hsv(img)

    # some images are shown in the wrong colors, but saved with the correct one
    # this fixes the issue, but fucks up the color for other images
    # if not img.flags['OWNDATA']:
    #     img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    return img


def show_image(image, title='', figsize=(12, 12)):
    image = load_image(image)

    plt_clear()
    plt.figure(figsize=figsize)
    if title:
        plt.title(title, fontsize=15)
    plt.axis('off')
    plt.imshow(image)
    plt.show()
    plt_clear()


def plt_clear():
    plt.clf()
    plt.cla()
    plt.close()


def compute_expanded_background_color(img, percentage, use_mode_instead_of_average=True):
    img = np.asarray(img)

    if percentage == 100:
        return (int(img[:, :, 0].mean()),
                int(img[:, :, 1].mean()),
                int(img[:, :, 2].mean()))
    else:
        h, w = img.shape[:2]
        horizontal_pixels = int(w * percentage / 100)
        vertical_pixels = int(h * percentage / 100)

        sides = (
            img[:vertical_pixels, :],  # top
            img[-vertical_pixels:, :],  # bottom
            img[:, :horizontal_pixels],  # left
            img[:, -horizontal_pixels:],  # right
        )

        if use_mode_instead_of_average:
            counter = defaultdict(int)
            if img.ndim == 2:
                for side in sides:
                    for color in find_distinct_pixels(side):
                        counter[color] += np.sum(np.all(side == color))
            else:
                for side in sides:
                    for color in find_distinct_pixels(side):
                        counter[color] += np.sum(np.all(side == color, axis=2))

            return max(counter.items(), key=lambda kv: kv[1])[0]
        else:
            red, green, blue = 0, 0, 0
            for side in sides:
                red += side[:, :, 0].mean() / 4
                green += side[:, :, 1].mean() / 4
                blue += side[:, :, 2].mean() / 4

            return (int(red), int(green), int(blue))


def find_distinct_pixels(image):
    if image.ndim == 3: # color
        return [tuple(v) for v in np.unique(image.reshape(-1, image.shape[2]), axis=0).tolist()]
    else: # grayscale or binary
        return np.unique(image).tolist()


def pad_np_granular(ndarr, pad_left, pad_right, pad_top, pad_bottom, bg=None):
    """WARNING: The returned image might have a different type or number of channels"""

    if ndarr.ndim >= 3:
        channels = ndarr.shape[2]
    else:
        channels = 1

    if bg is None:
        bg = compute_expanded_background_color(ndarr, 3)

    if type(bg) == int and channels > 1:
        bg = [bg] * channels

    h, w = ndarr.shape[:2]
    if channels > 1:
        result = (np.ones((h + pad_top + pad_bottom, w + pad_left + pad_right, channels), np.uint8) * bg).astype(np.uint8)
    else:
        result = (np.ones((h + pad_top + pad_bottom, w + pad_left + pad_right), np.uint8) * bg).astype(np.uint8)

    result[pad_top:pad_top + h, pad_left:pad_left + w] = ndarr
    return result


def concatenate_images(images, columns=None, figsize=None, dpi=300):
    plt_clear()
    n = len(images)

    if columns is None:
        if n <= 3:
            columns = n
        elif n == 4:
            columns = 2
        else:
            columns = 3

    grid = (math.ceil(n/columns), columns)
    figsize = figsize or (6 * grid[1], 4 * grid[0])
    fig, subplots = plt.subplots(*grid, figsize=figsize, dpi=dpi)
    fig.patch.set_facecolor('white')

    for i, (img, subplot) in enumerate(zip(images, fig.axes)):
        subplot.imshow(load_image(img))

        if type(img) == str:
            title = img.rpartition('.')[0] # remove extension
            title = title.rpartition('/')[2] # remove parent folder(s)
            subplot.set_title(title)

        # hide axis bars
        subplot.xaxis.set_visible(False)
        subplot.yaxis.set_visible(False)

    # remove the empty subplots
    for idx in range(i+1, grid[0] * grid[1]):
        fig.delaxes(subplots.flatten()[idx])

    return fig


def merge_image_with_mask(img, mask, color=[0, 255, 0], alpha=0.5, in_place=False):
    mask = mask.astype(bool)
    if not in_place:
        img = img.copy()

    img[mask] = img[mask] * (1 - alpha) + np.array(color) * alpha
    return img


def merge_image_with_masks(img, masks, color=[0, 255, 0], alpha=0.5, in_place=False):
    """You can optimize this by summing all masks, then applying the result to the image instead of adding each individual mask"""
    if not in_place:
        img = img.copy()

    for mask in masks:
        img = merge_image_with_mask(img, mask, color, alpha, in_place=True)

    return img


def merge_images(background, overlay, x=0, y=0):
    background_width = background.shape[1]
    background_height = background.shape[0]

    if x >= background_width or y >= background_height:
        return background # NOTE: shouldn't I raise an error here?

    result = background.copy()
    h, w = overlay.shape[:2]

    if x + w > background_width:
        w = background_width - x
        overlay = overlay[:, :w]

    if y + h > background_height:
        h = background_height - y
        overlay = overlay[:h]

    if overlay.ndim >= 3 and overlay.shape[2] < 4:
        overlay = np.concatenate(
            [
                overlay,
                np.ones((overlay.shape[0], overlay.shape[1], 1), dtype = overlay.dtype) * 255
            ],
            axis = 2,
        )

        overlay_image = overlay[..., :3]
        mask = overlay[..., 3:] / 255.0
    else:
        overlay_image = overlay
        mask = overlay / 255.0

    overlay_image = overlay_image.squeeze()
    result = result.squeeze()
    result[y:y+h, x:x+w] = (1.0 - mask) * result[y:y+h, x:x+w] + mask * overlay_image
    return result


def segment_channel_mask(A, interval, rescale_factor=1):
    # skimage uses floats in the range [0, 1] instead of [0, rescale_factor], so we have to transform our thresholds too
    a, b = np.array(interval) / rescale_factor

    segmented = np.zeros(A.shape)
    mask = (A >= a) & (A <= b)
    segmented[mask] = 1

    return segmented


def segment_channel(A, channel, interval, rescale_factor=1):
    mask = segment_channel_mask(A[:, :, channel], interval, rescale_factor)
    final = A.copy()
    final[mask == 0] = 0
    return final


def get_kernel(size, shape='rectangle'):
    if shape == 'rectangle':
        if type(size) != tuple:
            size = (size, size)

        kernel = np.ones(size)
    elif shape == 'top_left':
        kernel = np.flip(np.tri(size, dtype=np.uint8).T, 1)
    elif shape == 'top_right':
        kernel = np.tri(size, dtype=np.uint8).T
    elif shape == 'bottom_right':
        kernel = np.flip(np.tri(size, dtype=np.uint8), 1)
    elif shape == 'bottom_left':
        kernel = np.tri(size, dtype=np.uint8)
    elif shape == 'cross':
        assert size % 2 == 1, f'size must be odd for shape = {repr(shape)}'
        kernel = np.zeros((size, size), dtype=np.uint8)
        kernel[size//2, :] = 1
        kernel[:, size//2] = 1
    elif shape == 'circle':
        assert size % 2 == 1, f'size must be odd for shape = {repr(shape)}'
        size = size // 2
        x, y = np.ogrid[-size: size+1, -size: size+1]
        kernel = (x*x + y*y <= size**2).astype(np.uint8)
    elif shape == 'diamond':
        a = np.arange(size)
        b = np.minimum(a,a[::-1])
        kernel = ((b[:,None]+b) >= (size-1)//2).astype(np.uint8)

    return kernel


def ij_array_resize(A, size, offset=(0,0)):
    new_A = np.zeros(size)
    y, x = offset
    Y, X = (A.shape[0] + offset[0], A.shape[1] + offset[1])
    new_A[y:Y, x:X] = A
    return new_A


def ij_array_common_size(A, B):
    A, B = np.asarray(A), np.asarray(B)
    if A.shape != B.shape:
        (x, y), (x2, y2) = A.shape[:2], B.shape[:2]
        size = max(x, x2),  max(y, y2)
        A = ij_array_resize(A, size)
        B = ij_array_resize(B, size)

    return A, B


def ij_mask_intersection(A, B):
    A, B = ij_array_common_size(A, B)
    C = np.zeros(A.shape)
    C[(A != 0) & (B != 0)] = 1
    return C


def ij_mask_union(A, B):
    A, B = ij_array_common_size(A, B)
    C = np.zeros(A.shape)
    C[(A != 0) | (B != 0)] = 1
    return C


def ij_mask_difference(A, B):
    A, B = ij_array_common_size(A, B)
    C = np.zeros(A.shape)
    C[(A != 0) & (B == 0)] = 1
    return C


def ij_mask_symmetric_difference(A, B):
    return ij_mask_difference(
        ij_mask_union(A ,B),
        ij_mask_intersection(A ,B)
    )


def ij_RLE_encode(A):
    pixels = np.concatenate([[0], A.flatten(), [0]])
    runs = pixels[1:] != pixels[:-1][0]
    runs[1::2] -= runs[:-1:2]
    runs = runs.tolist()
    runs = izip(runs[:-1:2], runs[1::2])
    return runs


def ij_RLE_decode(rle, size):
    r, c = size[:2]
    A = np.zeros(r * c)
    for start, n in rle:
        A[start:start+n+1] = 1

    return A.reshape((r, c))


def morphology_gradient(img, kernel):
    # morph() will automatically pick the binary version when appropiate
    D = morph(img, kernel, 'D')
    E = morph(img, kernel, 'E')

    try:
        return D - E # grayscale
    except TypeError:
        return ij_mask_difference(D, E) # binary


def morph(img, kernel, mode, iterations=1):
    if img.ndim >= 3:
        img = load_image(img, 'gray')

    options = {
        ('B', False): morphology.black_tophat,
        ('B', True): morphology.black_tophat,
        ('C', False): morphology.closing,
        ('C', True): morphology.binary_closing,
        ('D', False): morphology.dilation,
        ('D', True): morphology.binary_dilation,
        ('E', False): morphology.erosion,
        ('E', True): morphology.binary_erosion,
        ('G', False): morphology_gradient,
        ('G', True): morphology_gradient,
        ('O', False): morphology.opening,
        ('O', True): morphology.binary_opening,
        ('W', False): morphology.white_tophat,
        ('W', True): morphology.white_tophat,
    }

    is_binary = np.array_equal(np.unique(img), [0, 1])
    morph_fn = options[(mode, is_binary)]
    for i in range(iterations):
        img = morph_fn(img, kernel)

    return img


def intersect_with_mask(A, mask):
    return A * mask


def hwc(A):
    h, w, *c = A.shape
    c = c[0] if c else 1
    return h, w, c


def get_distinct_pixels(A, return_counts=False):
    h, w, c = hwc(A)
    if c == 1:
        unique = np.unique(A, return_counts=return_counts)
    else:
        unique = np.unique(A.reshape((-1, c)), axis=0, return_counts=return_counts)

    if return_counts:
        pixels, counts = [arr.tolist() for arr in unique]
        if isinstance(pixels[0], list):
            pixels = [tuple(px) for px in pixels]

        return Counter(dict(zip(pixels, counts)))
    else:
        return unique


def draw_line_H(img, y, width=1, color=(0, 255, 0), in_place=False):
    if not in_place:
        img = img.copy()

    start, end = get_range_from_center_and_width(y, width)
    start = clip(start, 0, img.shape[0])
    end = clip(end, 0, img.shape[0])
    img[start:end] = color

    return img


def draw_line_V(img, x, width=1, color=(0, 255, 0), in_place=False):
    if not in_place:
        img = img.copy()

    start, end = get_range_from_center_and_width(x, width)
    start = clip(start, 0, img.shape[1])
    end = clip(end, 0, img.shape[1])
    img[:, start:end] = color

    return img


def draw_text(img, text, x, y, size=None, color=(0, 255, 0)):
    img = load_image(img)
    img = PIL_Image.fromarray(img)

    draw = ImageDraw.Draw(img)

    if size is None:
        size = int(5/100 * img.size[0])  # 5% of image height

    font = ImageFont.truetype("DejaVuSans.ttf", size)
    draw.text((x, y), text, color, font=font)

    return np.array(img)


def draw_bbox(image, box, color=(0, 255, 0), in_place=False):
    x, y, X, Y = box
    r = [y, y, Y, Y, y]
    c = [x, X, X, x, x]
    rr, cc = polygon_perimeter(r, c, image.shape)

    if not in_place:
        image = image.copy()

    image = image.copy()
    image[rr, cc] = color
    return image


def draw_bboxes(image, bboxes, color=(0, 255, 0), in_place=False):
    """You can optimize this by computing all perimeters and merging the rr, cc of polygon_perimeter() of each bbox
    and doing `image[rr, cc] = color` only at the end"""
    if not in_place:
        image = image.copy()

    for box in bboxes:
        # here we can set in_place=True because we copied the image before if in_place was False
        image = draw_bbox(image, box, color, in_place=True)

    return image


def get_pixels(image):
    """Return the unique pixels colors"""
    if image.ndim == 3:
        # RGB, RGBa, or HSV
        return [tuple(v) for v in np.unique(image.reshape(-1, image.shape[2]), axis=0).tolist()]
    else:
        # grayscale or binary
        return np.unique(image).tolist()


def get_numpy_info(A):
    return {
        'shape': A.shape,
        'c': hwc(A)[2],
        'pixels': get_pixels(A),
        'min': get_min_with_index(A),
        'max': get_max_with_index(A),
        'dtype': A.dtype,
    }


def get_min_with_index(A):
    """Returns the index as (i, j)"""
    if hwc(A)[2] in (3, 4):
        # for RGB or RGBa images
        # multiply pixel channels to get a single value (same as L2(px, (0,0,0))
        normed = np.linalg.norm(A, axis=2)
        idx = normed.argmin()
    else:
        # for grayscale
        idx = A.argmin()

    i, j = np.unravel_index(idx, A.shape[:2])
    return (A[i, j], (int(i), int(j)))


def get_max_with_index(A):
    """Returns the index as (i, j)"""
    if hwc(A)[2] in (3, 4):
        # for RGB or RGBa images
        # multiply pixel channels to get a single value (same as L2(px, (0,0,0))
        normed = np.linalg.norm(A, axis=2)
        idx = normed.argmax()
    else:
        # for grayscale
        idx = A.argmax()

    i, j = np.unravel_index(idx, A.shape[:2])
    return (A[i, j], (int(i), int(j)))
