import datetime
import re

from utils.data import is_number, is_str
from utils.regex import RX


def is_timedelta(x): return isinstance(x, datetime.timedelta)


def is_date(x): return isinstance(x, datetime.date)


def is_time(x): return isinstance(x, datetime.time)


def is_datetime(x): return isinstance(x, datetime.datetime)


def is_datetime_or_date_or_time(x): return isinstance(x, (datetime.datetime, datetime.date, datetime.time))


def is_timedelta(x): return isinstance(x, datetime.timedelta)


def get_datetime(dt=None, delta=None, unit='days', fmt=None):
    """if fmt is not None return the datetime as a string"""
    dt = dt or datetime.datetime.now()
    dt = datetime_as_obj(dt)

    if is_number(delta):
        if   unit == 'years': raise NotImplementedError('needs special handling')
        elif unit == 'months': raise NotImplementedError('needs special handling')
        elif unit == 'days': dt += datetime.timedelta(days=delta)
        elif unit == 'hours': dt += datetime.timedelta(hours=delta)
        elif unit == 'minutes': dt += datetime.timedelta(minutes=delta)
        elif unit == 'seconds': dt += datetime.timedelta(seconds=delta)
        elif unit in ['milliseconds', 'ms']: dt += datetime.timedelta(milliseconds=delta)
        elif unit in ['microseconds', 'us']: dt += datetime.timedelta(microseconds=delta)
    elif is_timedelta(delta):
        dt += delta

    if fmt:
        dt = str_from_datetime(dt, fmt)

    return dt


def day_of_year(dt=None):
    dt = get_datetime() if dt is None else datetime_as_obj(dt)
    return dt.timetuple().tm_yday


def day_of_week(dt=None):
    dt = get_datetime() if dt is None else datetime_as_obj(dt)
    return dt.isoweekday()


def week_of_year(dt=None):
    dt = get_datetime() if dt is None else datetime_as_obj(dt)
    dt = dt.isocalendar()
    if hasattr(dt, 'week'):
        return dt.week
    else:
        return dt[1]


def datetime_as_str(dt, fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt if is_str(dt) else str_from_datetime(dt, fmt)


def datetime_as_obj(dt, fmt=None):
    return dt if is_datetime_or_date_or_time(dt) else datetime_from_str(dt, fmt)


def str_from_datetime(dt, fmt='%Y-%m-%d %H:%M:%S.%f'):
    return dt.strftime(fmt)


def datetime_from_str(dt, fmt=None):
    if fmt is None:
        fmt = datetime_fmt_from_str(dt)

    return datetime.datetime.strptime(dt, fmt)


def change_datetime_format(dt, before=None, after=None):
    dt = datetime_from_str(dt, before)
    dt = str_from_datetime(dt, after)
    return dt


def datetime_split(dt):
    dt = datetime_as_obj(dt)
    year, month, day = dt.year, dt.month, dt.day
    h, m, s, ms = dt.hour, dt.minute, dt.second, dt.microsecond
    return (year, month, day), (h, m, s, ms)


def seconds_to_time(s):
    hours = int(s / 3600)
    minutes = int(s % 3600 / 60)
    seconds = int(s % 60)

    if hours:
        return f'{hours:02}:{minutes:02}:{seconds:02}'
    else:
        return f'{minutes:02}:{seconds:02}'


def datetime_fmt_from_str(dt):
    word_fmt = {
        'ISO': '%Y-%m-%dT%H:%M:%S.%fZ',
        'datetime': '%Y-%m-%d %H:%M:%S.%f',
        'date': '%Y-%m-%d',
        'time': '%H:%M:%S.%f',
    }

    if dt in word_fmt:
        return word_fmt[dt]
    elif re.search(RX.DATETIME_ISO, dt):
        return '%Y-%m-%dT%H:%M:%S.%fZ'
    elif re.search(RX.DATETIME_WITH_S_AND_MS, dt):
        return '%Y-%m-%d %H:%M:%S.%f'
    elif re.search(RX.DATETIME_WITH_S, dt):
        return '%Y-%m-%d %H:%M:%S'
    elif re.search(RX.DATETIME, dt):
        return '%Y-%m-%d %H:%M'
    elif re.search(RX.TIME_WITH_S_AND_MS, dt):
        return '%H:%M:%S.%f'
    elif re.search(RX.TIME_WITH_S, dt):
        return '%H:%M:%S'
    elif re.search(RX.TIME, dt):
        return '%H:%M'
    elif re.search(RX.DATE, dt):
        return '%Y-%m-%d'


def is_time_between(time, min_time=None, max_time=None):
    if min_time and max_time:
        ok_min = f'{time:>05}' >= f'{min_time:>05}'
        ok_max = f'{time:>05}' <  f'{max_time:>05}'
        return ok_min and ok_max
    else:
        raise ValueError('min_time and max_time needed')
