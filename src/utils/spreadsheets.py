import copy

from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.utils.cell import get_column_letter, column_index_from_string
from tqdm import tqdm

from utils.data import is_empty


def convert_col(c):
    if isinstance(c, int):
        return get_column_letter(c)
    else:
        return column_index_from_string(c)


def load_spreadsheet(src, has_header=True, as_json=True):
    if isinstance(src, Workbook):
        final = copy.deepcopy(src)
    elif isinstance(src, str):
        final = load_workbook(filename=src)
    elif isinstance(src, Worksheet):
        final = Workbook()
        sheet = final.create_sheet(src.title)
        copy_sheet_cells(src, sheet)
    elif is_empty(src):
        final = {'Sheet1': {'title': 'Sheet1', 'cells': [], 'header': None, 'range': None}}
    # elif is_list(src):
    # 	final = [load_spreadsheet()]
    # 				list of workbooks
    # 			list of worksheets
    # 			list of
    # 	elif is_dict(src[0]):
    # 		if set(src[0].keys()) == {'cells', 'header', 'range', 'sheet'}:
    # 			final = {s['title']: load_spreadsheet(s) for s in src}
    # 		else:
    # 			header = None
    # 			cell_range = None
    #
    # 			final = {'Sheet1': {
    # 				'title': 'Sheet1',
    # 				'cells': copy.deepcopy(src),
    # 				'header': header,
    # 				'range': cell_range
    # 			}}

    # if is_list(data):
    # 	sheet = src.create_sheet('Sheet1')
    # 	if data and is_dict(data[0]):
    # 		pass
    #
    # 	elif data and is_list((data[0])):
    # 		pass
    #
    # if is_dict(data):
    # 	src = Workbook()
    #
    # elif isinstance(src, Workbook):
    # 	custom = {sheet: {} for sheet in src.sheetnames}
    # else:
    # 	raise Exception(f'Unknown type {repr(type(src).__name__)}')
    #
    # for sheet in custom:
    # 	cell_range = src[sheet].dimensions
    # 	cells = [[cell.value for cell in row] for row in src[sheet][cell_range]]
    #
    # 	if has_header:
    # 		header, *cells = cells
    # 	else:
    # 		header = [c.column_letter for c in src[sheet][cell_range][0]]
    #
    # 	cells = [dict(zip(header, row)) for row in cells]
    #
    # 	custom[sheet] = {
    # 		'range': cell_range,
    # 		'header': header,
    # 		'title': src[sheet].title,
    # 		'cells': cells,
    # 	}

    return final


def save_spreadsheet(path, data):
    wb = load_spreadsheet(data, as_json=False)
    wb.save(filename=path)


def copy_sheet_attributes(src, dest):
    """SRC: https://stackoverflow.com/a/68800310/4515813"""
    dest.sheet_format = copy.deepcopy(src.sheet_format)
    dest.sheet_properties = copy.deepcopy(src.sheet_properties)
    dest.merged_cells = copy.deepcopy(src.merged_cells)
    dest.page_margins = copy.deepcopy(src.page_margins)
    dest.freeze_panes = copy.deepcopy(src.freeze_panes)
    dest._images = copy.deepcopy(src._images)

    # set row dimensions
    # So you cannot copy the row_dimensions attribute. Does not work (because of meta data in the attribute I think). So we copy every row's row_dimensions. That seems to work.
    for rn in range(len(src.row_dimensions)):
        dest.row_dimensions[rn] = copy.deepcopy(src.row_dimensions[rn])

    if src.sheet_format.defaultColWidth is None:
        print('Unable to copy default column wide')
    else:
        dest.sheet_format.defaultColWidth = copy.deepcopy(src.sheet_format.defaultColWidth)

    # set specific column width and hidden property
    # we cannot copy the entire column_dimensions attribute so we copy selected attributes
    for key, value in src.column_dimensions.items():
        dest.column_dimensions[key].min = copy.deepcopy(src.column_dimensions[key].min)   # Excel actually groups multiple columns under 1 key. Use the min max attribute to also group the columns in the targetSheet
        dest.column_dimensions[key].max = copy.deepcopy(src.column_dimensions[key].max)  # https://stackoverflow.com/questions/36417278/openpyxl-can-not-read-consecutive-hidden-columns discussed the issue. Note that this is also the case for the width, not onl;y the hidden property
        dest.column_dimensions[key].width = copy.deepcopy(src.column_dimensions[key].width) # set width for every column
        dest.column_dimensions[key].hidden = copy.deepcopy(src.column_dimensions[key].hidden)


def copy_sheet_cells(src, dest):
    """SRC: https://stackoverflow.com/a/68800310/4515813"""
    for (row, col), source_cell in src._cells.items():
        target_cell = dest.cell(column=col, row=row)

        target_cell._value = source_cell._value
        target_cell.data_type = source_cell.data_type

        if source_cell.has_style:
            target_cell.font = copy.deepcopy(source_cell.font)
            target_cell.border = copy.deepcopy(source_cell.border)
            target_cell.fill = copy.deepcopy(source_cell.fill)
            target_cell.number_format = copy.deepcopy(source_cell.number_format)
            target_cell.protection = copy.deepcopy(source_cell.protection)
            target_cell.alignment = copy.deepcopy(source_cell.alignment)

        if source_cell.hyperlink:
            target_cell._hyperlink = copy.deepcopy(source_cell.hyperlink)

        if source_cell.comment:
            target_cell.comment = copy.deepcopy(source_cell.comment)


def open_worksheet(workbook, worksheet):
    workbook.active = workbook[worksheet]
    return workbook.active


def get_header(ws, standardised=False):
    (r, c), (R, C) = get_spreadsheet_bbox(ws)

    if standardised:
        return [c.lower().strip() for c in get_header(ws, standardised=False)]
    else:
        # pyopenxl uses indexes starting with 1
        return [ws.cell(row=r+1, column=col+1).value for col in range(c, C+1)]


def get_spreadsheet_bbox(ws):
    r, R = get_min_max_rows(ws)
    c, C = get_min_max_cols(ws)
    return (r, c), (R, C)


def get_min_max_cols(ws):
    c, C = None, None
    for i, col in enumerate(ws.iter_cols()):
        col_values = set(cell.value for cell in col)
        if col_values != {None}:
            C = i
            if c is None:
                c = i

    return c, C


def get_min_max_rows(ws):
    r, R = None, None
    for i, row in enumerate(ws.iter_rows()):
        row_values = set(cell.value for cell in row)
        if row_values != {None}:
            R = i
            if r is None:
                r = i

    return r, R


def iter_rows(ws):
    (r, c), (R, C) = get_spreadsheet_bbox(ws)
    rows = ws.iter_rows()

    # skip empty rows and the header
    for i in range(r+1):
        next(rows)

    for i, row in enumerate(rows, start=i+1):
        if i <= R:
            yield [cell.value for cell in row[c:C+1]]


def iter_rows_with_keys(ws, header=None):
    (r, c), (R, C) = get_spreadsheet_bbox(ws)
    rows = ws.iter_rows()
    if header is None:
        header = get_header(ws)

    # skip empty rows and the header
    for i in range(r+1):
        next(rows)

    for i, row in enumerate(rows, start=i+1):
        if i <= R:
            yield {header[j]: cell.value for j, cell in enumerate(row[c:C+1])}


def iter_cols(ws):
    (r, c), (R, C) = get_spreadsheet_bbox(ws)
    cols = ws.iter_cols()

    # skip empty cols
    for i in range(c):
        next(cols)

    for i, col in enumerate(cols, start=i+1):
        if i <= C:
            # `r+1` skips the header
            yield [cell.value for cell in col[r+1:R+1]]
