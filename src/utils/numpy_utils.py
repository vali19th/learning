import numpy as np


def np_where_nested(A, pairs):
    """
    pairs has the form [(c1, t1), (c2, t2), ..., (cn, tn, else)], c = condition, t = then

    Equivalent with
    np.where(c1, x1,
        np.where(c2, x2,
            np.where(c3, x3, ...)))

    x = np.array([1, 2, 3, 4, 5, 6, 7])
    x = np_where_nested(x, [(x == 1, -1), (x <= 2, -2), (x <= 3, -3), (x <= 4, -4, 0)])
    x  # == [-1, -2, -3, -4, 0, 0, 0]
    """
    assert len(pairs[-1]) == 3
    assert all(len(p) == 2 for p in pairs[:-1])

    mask_keep = np.array([False] * len(A))
    for condition, then in pairs[:-1]:
        mask_then = ~mask_keep & condition
        A = np.where(mask_then, then, A)
        mask_keep = mask_keep | condition

    condition, then, else_ = pairs[-1]
    mask_then = ~mask_keep & condition
    mask_else = ~mask_keep & ~condition
    A = np.where(mask_then, then, A)
    A = np.where(mask_else, else_, A)

    return A
