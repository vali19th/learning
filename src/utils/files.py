import warnings
import datetime
import re
import csv
import xml
import json
import pickle
import shutil
import os

from pathlib import Path
from ftplib import FTP

import pandas as pd
import xmltodict

from utils.data import depth_map, is_bytes, is_iterable, is_pandas_df, is_str, is_tuple, remove_done
from utils.datetime_utils import is_datetime_or_date_or_time


def is_ftp(path):
    return is_tuple(path) and isinstance(path[0], FTP) and is_str(path[1])


def ftp_and_string_from_path(path):
    return path if is_ftp(path) else (None, path)


def str_from_path(path):
    return ftp_and_string_from_path(path)[1]


def ftp_from_path(path):
    return ftp_and_string_from_path(path)[0]


def is_file(path, raise_if_not_exists=True):
    if not path_exists(path):
        if raise_if_not_exists:
            raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')
        else:
            return False

    if is_ftp(path):
        parent, _, head = path_rpartition(path)
        files_and_dirs = ls(parent, with_type=True)
        return (head, 'file') in files_and_dirs
    else:
        return os.path.isfile(path)


def is_dir(path):
    return not is_file(path)


def is_empty_dir(path):
    return is_dir(path) and (ls(path) == [])


def path_exists(path):
    def path_exists_ftp(ftp, path):
        if path in ('', '.'):
            return True

        parent, _, head = path_rpartition((ftp, path))
        if parent == (ftp, ''):
            parent = (ftp, '.')

        if '/' in path and parent != (ftp, '.') and not path_exists(parent):
            return False

        return head in ls(parent)

    if is_ftp(path):
        return path_exists_ftp(*path)
    else:
        if path in ('', '.'):
            return True

        return os.path.exists(path)


def path_split(path, separator=' '):
    if is_ftp(path):
        ftp, just_path = path
        return [(ftp, piece) for piece in just_path.split(separator)]
    else:
        return path.split(separator)


def path_join(iterable, separator=''):
    if iterable and is_iterable(iterable):
        if is_ftp(iterable[0]):
            ftp, _ = iterable[0]
            return (ftp, separator.join(piece for _, piece in iterable))
        else:
            return separator.join(iterable)
    else:
        return ''


def path_rpartition(path, separator='/'):
    if is_ftp(path):
        ftp, just_path = path
        return [(ftp, piece) for piece in just_path.rpartition(separator)]
    else:
        return path.rpartition(separator)


def path_append(path, suffix):
    if is_ftp(path):
        ftp, just_path = path
        return (ftp, just_path + str_from_path(suffix))
    else:
        return path + str_from_path(suffix)


def path_prepend(path, prefix):
    if is_ftp(path):
        ftp, just_path = path
        return (ftp, str_from_path(prefix) + just_path)
    else:
        return str_from_path(prefix) + path


def path_replace(path, before, after):
    ftp_path, path = ftp_and_string_from_path(path)
    new_path = path.replace(str_from_path(before), str_from_path(after))
    if ftp_path:
        return (ftp_path, new_path)
    else:
        return new_path


def filename_without_extension(path):
    return os.path.splitext(os.path.basename(path))[0]


def read_all(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    contents = {}
    for f in get_files(path, **kwargs):
        filename = path_to_filename(f, **kwargs)
        contents[filename] = read(f, **kwargs)

    return contents


def read(path, default=FileNotFoundError, as_type=None, **kwargs):
    """if a default is provided, ignore the error and return the default"""
    read_ = {
        'binary': read_binary,
        'csv': read_csv,
        'json': read_json,
        'pkl': load_pkl,
        'sql': read_sql,
        'txt': read_text,
        'xml': read_xml,
    }

    if as_type:
        assert as_type in read_, f'Unknown type {repr(as_type)}'
    else:
        as_type = path.rpartition('.')[-1]
        if as_type not in read_:
            warnings.warn(f'Unknown type {repr(as_type)}. The file will be read as text.')
            as_type = 'txt'

    try:
        return read_[as_type](path, **kwargs)
    except FileNotFoundError:
        if default == FileNotFoundError:
            raise
        else:
            return default


def write(path, data, as_type=None, **kwargs):
    write_ = {
        'binary': write_binary,
        'csv': write_csv,
        'json': write_json,
        'pkl': save_pkl,
        'sql': write_sql,
        'txt': write_text,
        'xml': write_xml,
    }

    if as_type:
        assert as_type in write_, f'Unknown type {repr(as_type)}'
    else:
        as_type = path.rpartition('.')[-1]
        if as_type not in write_:
            if is_str(data):
                warnings.warn(f'Unknown type {repr(as_type)}. The file will be written as text.')
                as_type = 'txt'
            elif is_bytes(data):
                warnings.warn(f'Unknown type {repr(as_type)}. The file will be written as binary.')
                as_type = 'binary'
            else:
                raise NotImplementedError(f'Unknown type {repr(as_type)}')

    return write_[as_type](path, data, **kwargs)


def read_xml(path, fix_errors=True, **kwargs):
    data = read(path, as_type='txt')

    try:
        return xmltodict.parse(data)
    except Exception as e:
        if fix_errors:
            """ TODO:
                How do I make sure I fix error combos?
                - while loop
                    - with list of errors initialised before the loop
                    - have a private function for solving each error and combine them into a pipeline
            """
            if isinstance(e, xml.parsers.expat.ExpatError) and str(e).startswith('junk after document element:'):
                warnings.warn(f'The xml seemed to be missing the root and a fake one was used. If you want an error to be raised, call with fix_errors=False')
                return xmltodict.parse(f'<ROOT>{data}</ROOT>')
            else:
                raise e
        else:
            raise e


def write_xml(path, data, **kwargs):
    data = xmltodict.unparse(data, pretty=True)
    write(path, data, as_type='txt')


def read_sql(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    return re.sub(r'([^:]):(\w+)\b', r'\1%(\2)s', read_text(path)).strip()


def write_sql(path, sql, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    write_text(path, data=re.sub(r'\%\((\w+)\)s', r':\1', sql).strip())


def write_text(path, data, *, mode='w', **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, mode, encoding='utf-8') as f:
        f.write(data)


def read_text(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'r', encoding='utf-8') as f:
        return f.read()


def read_binary(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'rb') as f:
        return f.read()


def write_binary(path, data, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'wb') as f:
        f.write(data)


def read_json(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'r', encoding='utf-8') as f:
        return json.load(f)


def write_json(path, data, *, indent=4, minified=False, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    data = depth_map(str, data, condition=is_datetime_or_date_or_time)
    with open(path, 'w', encoding='utf-8') as f:
        if minified:
            json.dump(data, f, indent=None, separators=(',', ':'))
        else:
            json.dump(data, f, indent=indent)


def read_csv(path, *, delimiter=',', quotechar='"', **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    if kwargs.get('pandas'):
        return pd.read_csv(path)
    else:
        with open(path, 'r', encoding='utf-8') as f:
            data = list(csv.DictReader(f, delimiter=delimiter, quotechar=quotechar))

        return data


def write_csv(path, data, *, keys=None, delimiter=',', quotechar='"', **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    if is_pandas_df(data):
        data.to_csv(path, index=False)
    else:
        keys = keys or data[0].keys()
        with open(path, 'w', encoding='utf-8') as f:
            out = csv.DictWriter(f, fieldnames=keys, delimiter=delimiter, quotechar=quotechar, quoting=csv.QUOTE_NONNUMERIC)
            out.writeheader()

            for item in data:
                out.writerow(item)


def load_pkl(path, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'rb') as f:
        pkl = pickle.load(f)

    return pkl


def save_pkl(path, data, protocol=4, **kwargs):
    """Use protocol=5 only for py3.8 or greater"""
    if is_ftp(path):
        raise NotImplementedError

    with open(path, 'wb') as f:
        pickle.dump(data, f, protocol=protocol)


def path_to_filename(path, *, split_extension=False, **kwargs):
    if is_ftp(path):
        raise NotImplementedError

    path = path.split(os.path.sep)[-1]
    if split_extension:
        path = path.split('.')
        if len(path) == 1:  # normal file w/o extension
            return (path[0], '')
        elif len(path) == 2 and path[0] == '':  # hidden file w/o extension
            return '.'.join(path)
        else:
            return ('.'.join(path[:-1]), path[-1])
    else:
        return path


def paths_to_filenames(paths, *, split_extension=False, **kwargs):
    return [path_to_filename(p, split_extension=split_extension) for p in paths]


def label_if_ftp(path):
    if is_ftp(path):
        return f'FTP:{str_from_path(path)}'
    else:
        return path


def get_filesize(path):
    if is_dir(path):
        raise ValueError(f'path = {repr(path)} is a dir')

    if is_ftp(path):
        try:
            ftp, just_path = path
            return ftp.size(just_path)
        except Exception as e:
            if repr(e) == "error_perm('550 SIZE not allowed in ASCII mode')":
                parent_dir, _, head = path_rpartition(path, '/')
                for file, size in ls(parent_dir, with_size=True):
                    if file == head:
                        return int(size)
            else:
                raise e
    else:
        return os.path.getsize(path)


def tree(path='.', separate_files_and_dirs=False):
    def tree_ftp(path):
        all_files = []
        all_dirs = {path}
        visited_dirs = set()
        while all_dirs != visited_dirs:
            for ftp, dir_to_visit in all_dirs - visited_dirs:
                visited_dirs.add((ftp, dir_to_visit))
                for item, item_type in ls((ftp, dir_to_visit), with_type=True, as_paths=True):
                    if item_type == 'file':
                        all_files.append(item)
                    else:
                        all_dirs.add(item)

        return all_files, all_dirs

    if is_ftp:
        files, dirs = tree_ftp(path)
    else:
        files = get_files(path, recursive=True)
        dirs = get_dirs(path, recursive=True)

    if separate_files_and_dirs:
        return sorted(files), sorted(dirs)
    else:
        return sorted(files + list(dirs))


def ls(path='.', with_type=False, with_size=False, as_paths=False):
    """if as_paths=True, return paths (e.g. 'a/b/c/image.png') instead of just the dir/filename (e.g. 'image.png')"""

    def ls_ftp(ftp, path='.', with_type=False, with_size=False):
        facts = []
        if with_type: facts.append('type')
        if with_size: facts.append('size')

        if path == '':
            path = '.'

        result = sorted(ftp.mlsd(path, facts=facts))

        if len(facts) == 0:
            result = [(ftp, p) for p, _ in result if p not in ['.', '..']]
        elif len(facts) == 1:
            result = [((ftp, p), list(t.values())[0]) for p, t in result if p not in ['.', '..']]
        else:
            result = [((ftp, p), t['type'], t['size']) for p, t in result if p not in ['.', '..']]

        return result

    if not path_exists(path):
        raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')

    if is_ftp(path):
        result = ls_ftp(*path, with_type, with_size)
    else:
        _, dirs, files = next(os.walk(path))

        if with_type and with_size:
            dirs = [(p, 'dir', None) for p in dirs]
            files = [(p, 'file', get_filesize(f'{path}/{p}')) for p in files]
        elif with_type:
            dirs = [(p, 'dir') for p in dirs]
            files = [(p, 'file') for p in files]
        elif with_size:
            dirs = [(p, None) for p in dirs]
            files = [(p, get_filesize(f'{path}/{p}')) for p in files]

        result = sorted(dirs + files)

    if as_paths and str_from_path(path) not in ('', '.', './'):
        result_2 = []
        for item in result:
            if is_tuple(item):
                if is_ftp(item):
                    ftp, p = item
                    result_2.append((ftp, f'{str_from_path(path)}/{p}'))
                elif is_ftp(item[0]):
                    (ftp, p), *rest = item
                    result_2.append(((ftp, f'{str_from_path(path)}/{p}'), *rest))
                else:
                    p, *rest = item
                    result_2.append((f'{path}/{p}', *rest))
            else:
                result_2.append(f'{path}/{item}')

        return result_2
    else:
        return result


def get_files(path, *, recursive=False, extension='', as_paths=False):
    """as_paths=False is ignored when recursive=True"""
    if not path_exists(path):
        raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')

    if is_ftp(path):
        if recursive:
            return tree(path, separate_files_and_dirs=True)[0]
        else:
            files = (item_path for item_path, item_type in ls(path, with_type=True, as_paths=as_paths) if item_type == 'file')

            if extension != '':
                files = ((ftp, fn) for ftp, fn in files if fn.split('.')[-1] == extension)

            return sorted(files)
    else:
        if recursive:
            return sorted(
                os.path.join(dp, f)
                for dp, dn, fn in os.walk(os.path.expanduser(path))
                for f in fn if extension == '' or f.rpartition('.')[2] == extension
            )
        else:
            files = (item_path for item_path, item_type in ls(path, with_type=True, as_paths=as_paths) if item_type == 'file')

            if extension != '':
                files = (f for f in files if f.split('.')[-1] == extension)

            return sorted(files)


def get_dirs(path, *, recursive=False, as_paths=False):
    """as_paths=False is ignored when recursive=True"""
    if not path_exists(path):
        raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')

    if is_ftp(path):
        if recursive:
            return tree(path, separate_files_and_dirs=True)[1]
        else:
            return sorted(item_path for item_path, item_type in ls(path, with_type=True, as_paths=as_paths) if item_type == 'dir')
    else:
        if recursive:
            return [path] + sorted(os.path.join(dp, d) for dp, dn, fn in os.walk(os.path.expanduser(path)) for d in dn)
        else:
            return sorted(item_path for item_path, item_type in ls(path, with_type=True, as_paths=as_paths) if item_type == 'dir')


def mkdir(path, exist_ok=True):
    def mkdir_ftp(ftp, path, exist_ok=True):
        if path in ('', '.'):
            pass
        elif path_exists((ftp, path)):
            if not exist_ok:
                raise FileExistsError(f'[Errno 17] File exists: {repr(path)}')
        else:
            parent, _, head = path.rpartition('/')
            if parent != '' and not path_exists((ftp, parent)):
                mkdir((ftp, parent))

            if not path_exists((ftp, path)):
                ftp.mkd(path)

    if path in ('', '.'):
        pass
    elif is_ftp(path):
        mkdir_ftp(*path, exist_ok)
    else:
        os.makedirs(path, exist_ok=exist_ok)


def delete(path, missing_ok=True):
    def delete_ftp(ftp, path):
        if path_exists((ftp, path)):
            if is_file((ftp, path)):
                ftp.delete(path)
            else:
                if is_empty_dir((ftp, path)):
                    ftp.rmd(path)
                else:
                    for item in sorted(tree((ftp, path)), reverse=True):
                        delete(item)
        else:
            if missing_ok:
                return
            else:
                raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')

    if path_exists(path):
        if is_ftp(path):
            delete_ftp(*path)
        else:
            if is_file(path):
                Path(path).unlink()
            else:
                shutil.rmtree(path)
    else:
        if missing_ok:
            return
        else:
            raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(path)}')


def move_file(src, dest, bs=8192):
    mkdir(path_rpartition(dest, '/')[0], exist_ok=True)

    if is_ftp(src) and is_ftp(dest):
        ftp, src = src
        ftp, dest = dest
        ftp.rename(src, dest)
    elif is_ftp(src) or is_ftp(dest):
        # one local, one ftp (the order does not matter)
        copy_file(src, dest, bs=bs)
        delete(src)
    else:
        # both local
        shutil.move(src, dest)


def copy_file(src, dest, bs=8192):
    mkdir(path_rpartition(dest, '/')[0], exist_ok=True)

    if is_ftp(src) and is_ftp(dest):
        ftp, src = src
        ftp, dest = dest
        local_path = src.replace('/', '-') + '.' + datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S.%s')
        copy_file((ftp, src), local_path)  # download
        copy_file(local_path, (ftp, dest))  # upload
        delete(local_path)  # cleanup
    elif is_ftp(src):
        ftp, src = src
        with open(dest, 'wb') as f:
            ftp.retrbinary(f'RETR {src}', f.write, blocksize=bs)
    elif is_ftp(dest):
        ftp, dest = dest
        with open(src, 'rb') as f:
            ftp.storbinary(f'STOR {dest}', f, blocksize=bs)
    else:
        shutil.copy2(src, dest)


def move_to_dir(src, dest):
    if is_file(src):
        mkdir(dest)
        fn = path_rpartition(src, '/')[2]
        move_file(src, path_append(dest, '/' + str_from_path(fn)))
    elif is_dir(src):
        # append the last directory from src to dest, so you move it too, not just its contents
        last_dir = str_from_path(path_split(src, '/')[-1])
        dest = path_append(dest, '/' + last_dir)

        for dirname in get_dirs(src, recursive=True): mkdir(path_replace(dirname, src, dest))
        for fn in get_files(src, recursive=True): move_file(fn, path_replace(fn, src, dest))
        delete(src)
    else:
        raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(src)}')


def copy_to_dir(src, dest, bs=8192):
    if is_file(src):
        mkdir(dest)
        fn = path_rpartition(src, '/')[2]
        copy_file(src, path_append(dest, '/' + str_from_path(fn)), bs=bs)
    elif is_dir(src):
        # append the last directory from src to dest, so you copy it too, not just its contents
        last_dir = str_from_path(path_split(src, '/')[-1])
        dest = path_append(dest, '/' + last_dir)

        for dirname in get_dirs(src, recursive=True): mkdir(path_replace(dirname, src, dest))
        for fn in get_files(src, recursive=True): copy_file(fn, path_replace(fn, src, dest), bs=bs)
    else:
        raise FileNotFoundError(f'[Errno 2] No such file or directory: {repr(src)}')


def ls_without_done(IN, done):
    todo = ls(IN)
    if is_iterable(done):
        done = paths_to_filenames(done)
    elif is_dir(done):
        done = ls(done)
    elif is_file(done):
        done = read(done)
        if done.endswith('.txt'):
            done = done.splitlines()

        done = paths_to_filenames(done)
    else:
        raise ValueError(f'Unkown case for done = {repr(done)}')

    if todo and done:
        # extract the extension
        # `partition()` correctly extracts extensions with multiple parts like .tar.gz
        ext_in = todo[0].partition('.')[2]
        ext_out = done[0].partition('.')[2]

        done = [p.replace(ext_out, ext_in) for p in done]
        todo = remove_done(todo, done)

    return [f'{IN}/{fn}' for fn in todo]
