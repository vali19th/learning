"""
This module will not respect the opencv conventions.
The interface will match what python programmers expect, not what opencv programmers expect.
    - function params and return values
    - the naming conventions
"""

import warnings

import cv2

from utils.data import is_int, is_number, is_str, running_window_lead_or_lag
from utils.files import ls, mkdir
from utils.images import hwc, show_image
from utils.sys_utils import get_frame_info


def get_video_images(path, *, nth_image=1, per_second=None, per_video=None, mode='RGB'):
    """
    - Priority: per_video -> per_second -> nth_image
    - BUG: for some values of per_second and per_video it yields more images than required
    """
    def update_nth_image(n, nth_image, per_second, per_video, video):
        if per_video is not None:
            if per_video < 1:
                raise ValueError(f'Expected per_video >= 1 or None, got {per_video!r} instead')
            elif n == -1:
                raise ValueError(f'Expected per_video == None when using a webcam, got {per_video!r} instead')

            nth_image = int(n / per_video)
        elif per_second is not None:
            if per_second <= 0:
                raise ValueError(f'Expected per_second > 0 or None, got {per_second!r} instead')

            fps = get_video_fps(video)
            nth_image = max(1, int(fps / per_second))
        else:
            if not (is_number(nth_image) and nth_image > 0):
                raise ValueError(f'Expected nth_image > 0, got {nth_image!r} instead')

            nth_image = max(1, int(nth_image))

        return nth_image

    video = open_video(path)
    n = get_video_number_of_images(video)
    nth_image = update_nth_image(n, nth_image, per_second, per_video, video)

    i = 0
    while True:
        success, image = video.read()
        if not success:
            if n >= 1 and i < n:
                warnings.warn(f'{get_frame_info()} Could not extract image #{i} from {path}')
            break

        if i % nth_image == 0:
            if mode == 'BGR':
                pass
            elif mode == 'RGB':
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            elif mode == 'gray':
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            else:
                raise ValueError(f'Unknown mode = {mode!r}')

            yield (video, i, image)

        i += 1

    video.release()


def extract_video_images(video, output_dir, *, nth_image=1, per_second=None, per_video=None):
    """TODO: change {i:>06} based on number of frames in video: len(str(num_of_frames))"""
    already_extracted = {p for p in ls(output_dir, as_paths=True) if video in p}
    video_fn = video.rpartition('/')[-1]
    prefix = f'{output_dir}/{video_fn}'

    for video, i, image in get_video_images(video, nth_image=nth_image, per_second=per_second, per_video=per_video, mode='BGR'):
        path = f'{prefix}_{i:>06}.png'
        if path not in already_extracted:
            cv2.imwrite(path, image)


def save_video(path, images, *, fps, mode='RGB'):
    mkdir(path.rpartition('/')[0])
    images = (to_BGR(image, mode) for image in images)

    image = next(images)
    height, width, channels = hwc(image)
    cc = cv2.VideoWriter_fourcc(*'MJPG')
    video = cv2.VideoWriter(path, cc, fps, (width, height))

    video.write(image)
    for image in images:
        video.write(image)

    cv2.destroyAllWindows()
    video.release()


def to_BGR(image, mode):
    if mode == 'BGR':
        return image
    elif mode == 'RGB':
        return cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    else:
        raise NotImplementedError(f'Unknown mode: {mode!r}')


def merge_images_into_video(input_dir, output_video, n, fps):
    images = sorted(ls(input_dir), key=lambda x: (len(x), x))
    images = images[::n]  # keep 1/n frames

    # video fps and resolution
    height, width, layers = cv2.imread(f'{input_dir}/{images[0]}').shape
    cc = cv2.VideoWriter_fourcc(*'MJPG')
    video = cv2.VideoWriter(output_video, cc, fps, (width, height))

    for image in images:
        video.write(cv2.imread(f'{input_dir}/{image}'))

    cv2.destroyAllWindows()
    video.release()


def get_video_resolution(path):
    video = open_video(path)
    return video.get(cv2.CAP_PROP_FRAME_WIDTH), video.get(cv2.CAP_PROP_FRAME_HEIGHT)


def get_video_fps(video):
    if is_str(video):
        video = open_video(video)
    return video.get(cv2.CAP_PROP_FPS)


def get_video_number_of_images(video):
    if is_str(video):
        video = open_video(video)
    return int(video.get(cv2.CAP_PROP_FRAME_COUNT))


def open_video(path):
    video = cv2.VideoCapture(path)
    assert video.isOpened(), f'Could not open {path!r}'
    return video


def cv_exit(wait=1):
    return cv2.waitKey(wait) & 0xFF == ord('q')


def process_and_show_video(
    video,
    process_fn,
    *,
    title=None,
    wait=1,
    images_per_loop=1,
    process_fn_args=None,
    process_fn_kwargs=None,
    get_video_images_args=None,
    get_video_images_kwargs=None
):
    title = (title or video)
    stream = process_video(
        video,
        process_fn,
        images_per_loop=images_per_loop,
        process_fn_args=process_fn_args,
        process_fn_kwargs=process_fn_kwargs,
        get_video_images_args=get_video_images_args,
        get_video_images_kwargs=get_video_images_kwargs
    )
    show_video(stream, title=title, wait=wait)


def process_and_save_video(
    video,
    process_fn,
    *,
    path,
    fps=None,
    mode='RGB',
    images_per_loop=1,
    process_fn_args=None,
    process_fn_kwargs=None,
    get_video_images_args=None,
    get_video_images_kwargs=None
):
    fps = (fps or get_video_fps(video))
    stream = process_video(
        video,
        process_fn,
        images_per_loop=images_per_loop,
        process_fn_args=process_fn_args,
        process_fn_kwargs=process_fn_kwargs,
        get_video_images_args=get_video_images_args,
        get_video_images_kwargs=get_video_images_kwargs
    )
    images = (image for _, _, image in stream)
    save_video(path, images, fps=fps, mode=mode)


def show_video(stream, *, title='', wait=1, mode='RGB'):
    for video, _, image in stream:
        cv2.imshow(str(title), to_BGR(image, mode))
        if cv_exit(wait):
            break

    video.release()
    cv2.destroyAllWindows()


def process_video(
    video,
    process_fn,
    *,
    images_per_loop=1,
    process_fn_args=None,
    process_fn_kwargs=None,
    get_video_images_args=None,
    get_video_images_kwargs=None
):
    if not(is_int(images_per_loop) and images_per_loop >= 1):
        raise ValueError(f'<images_per_loop> expects a positive integer. Got {images_per_loop!r}')

    process_fn_args = process_fn_args or []
    process_fn_kwargs = process_fn_kwargs or {}
    get_video_images_args = get_video_images_args or []
    get_video_images_kwargs = get_video_images_kwargs or {}

    stream = get_video_images(video, *get_video_images_args, **get_video_images_kwargs)
    if images_per_loop == 1:
        for video, i, image in stream:
            yield (video, i, process_fn(image, *process_fn_args, **process_fn_kwargs))
    else:
        stream = running_window_lead_or_lag(stream, images_per_loop)
        for window in stream:
            video, i, _ = window[0]
            images = [image for _, _, image in window]
            yield (video, i, process_fn(*images, *process_fn_args, **process_fn_kwargs))
