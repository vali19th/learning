import ast
import re
from collections import Counter, defaultdict

import matplotlib.pyplot as plt

import math

from shapely.geometry import Polygon

from utils.data import flatten_counter, is_float, is_list_or_tuple, is_number, is_str
from utils.datetime_utils import datetime_as_str, datetime_from_str, is_datetime
from utils.regex import RX


def plt_clear():
    plt.clf()
    plt.cla()
    plt.close()


def IoU(poly_1, poly_2):
    if is_list_or_tuple(poly_1): poly_1 = Polygon(poly_1)
    if is_list_or_tuple(poly_2): poly_2 = Polygon(poly_2)
    union_area = poly_1.union(poly_2).area
    if union_area:
        return poly_1.intersection(poly_2).area / union_area
    else:
        return 0


def intersection_over_poly(poly_1, poly_2):
    if is_list_or_tuple(poly_1): poly_1 = Polygon(poly_1)
    if is_list_or_tuple(poly_2): poly_2 = Polygon(poly_2)
    poly_area = poly_1.area
    intersection_area = poly_1.intersection(poly_2).area

    if intersection_area == 0:
        return 0
    elif poly_area == 0:
        return math.inf
    else:
        return intersection_area / poly_area


def hist(data, n=None, fn=None):
    # convert to list of tuples if necessaryg
    if type(data) == Counter:
        data = dict(data).items()
    elif type(data) == dict:
        data = data.items()

    # sort by frequency
    data = sorted(data, key=lambda xi: xi[1], reverse=True)

    if n: data = data[:n]

    plt_clear()
    plt.bar(*zip(*data))
    plt.xticks(rotation=-45)
    if fn:
        plt.savefig(fn)
    plt.show()


def counter_stats(counter):
    mc = counter.most_common(len(counter))
    total = sum(count for px, count in mc)
    cumulative = 0

    stats = {}
    for i, (px, c) in enumerate(mc):
        r = c/total
        cumulative += r
        stats[px] = {
            'i': i,
            'absolute': c,
            'relative': r,
            'cumulative': cumulative,
        }

    return stats


def min_max(x):
    return min(x), max(x)


def median(x):
    if len(x) % 2:
        return sorted(x)[int(len(x)/2)]
    else:
        mid = int(len(x)/2)
        return sum(sorted(x)[mid-1:mid+1])/2


def mode(x):
    c = Counter(x)
    max_n = c.most_common(1)[0][1]
    most_common = [k for k, n in c.items() if n == max_n]
    return sum(most_common) / len(most_common)


def mean(x):
    if isinstance(x, dict):
        return sum(k * v for k,v in x.items()) / (sum(x.values()) or 1)
    else:
        return sum(x) / (len(x) or 1)


def geometric_mean(x):
    return math.prod(x) ** (1/len(x))


def harmonic_mean(x):
    return len(x) / sum(1/xi for xi in x if xi != 0)


def aggregate(x):
    min_data, max_data = min_max(x)

    return {
        'min': min_data,
        'max': max_data,
        'sum': sum(x),

        'mean': mean(x),
        'geometric_mean': geometric_mean(x),
        'harmonic_mean': harmonic_mean(x),
        'mode': mode(x),
        'median': median(x),
    }


def remove_outliers(data, percentage, column=None):
    if column:
        ordered = sorted(((xi, stats) for xi, stats in data.items()), key=lambda item: item[1][column])
    else:
        ordered = sorted(((xi, stats) for xi, stats in data.items()), key=lambda item: item[0])

    i = 0  # avoid UnboundLocalError
    for i, (px, stats) in enumerate(ordered):
        percentage -= stats['relative']
        if percentage < 0:
            break

    return dict(ordered[i:])


def standardise_value(v, decimals=None, datetime_as_obj=False):
    def _str(v):
        v = v.strip().lower()

        if v in ('null', ''):
            return None
        elif v == 'false':
            return False
        elif v == 'true':
            return True
        elif v.startswith('='):
            return '<excel formula>'
        elif v.startswith('http'):
            match = re.search('(https?://[^/]+)', v)
            if match:
                return match.group(1)
            else:
                return '<link>'
        elif datetime_as_obj and re.search(RX.exact(RX.DATETIME_OR_DATE_OR_TIME), v):
            return datetime_from_str(v)
        else:
            return v

    def _float(v):
        if decimals is not None:
            return round(v, decimals)

    def _datetime(v):
        if not datetime_as_obj:
            return datetime_as_str(v)

    try:
        v = ast.literal_eval(v)
    except Exception:
        pass

    if is_str(v):
        return _str(v)
    elif is_float(v):
        return _float(v)
    elif is_datetime(v):
        return _datetime(v)
    else:
        return v


def get_distribution(data):
    distribution = defaultdict(Counter)
    for row in data:
        for k, v in row.items():
            v = standardise_value(v)
            distribution[k][v] += 1

    return distribution


def explore_distribution(data):
    def _get_meta_stats(x):
        if all(is_number(xi) for xi in x.keys()):
            agg = aggregate(flatten_counter(x))
            agg = sorted(agg.items(), key=lambda kv: kv[1], reverse=True)
            agg = [(f'[{k}]', f'{round(v, 2):.2f}') for k, v in agg]
            agg.append(['-----', '-----'])
            return agg

        return []

    for k, counter in sorted(data.items()):
        ordered = sorted(counter.items(), key=lambda kv: kv[1], reverse=True)
        meta = _get_meta_stats(counter)

        k = f'--- {k} '
        print(f'\n{k:-<40}')
        for value, n in meta + ordered:
            print(f'{n: >10} | {value}')
