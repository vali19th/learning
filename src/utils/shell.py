import subprocess


def shell(cmd, **kwargs):
    return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8').stdout.strip()


def clear_screen():
    print(chr(27) + "[2J")
