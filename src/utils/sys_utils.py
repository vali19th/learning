import datetime
import inspect
import subprocess
from pprint import pprint

from tqdm import tqdm

from utils.data import is_iterable
from utils.files import ls, mkdir


def shell(cmd, check=True, return_code=False):
    r = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8', check=check)
    output = r.stdout.strip()

    if return_code:
        return (output, r.returncode)
    else:
        return output


def run_script(cmd, **kwargs):
    return subprocess.run(cmd, shell=True)


def clear_screen():
    print(chr(27) + "[2J")


def wget(url, directory=None, timeout=60, tries=3, skip_done=True, throw_error=True):
    def is_done(url):
        fn = url.split('/')[-1]
        fn = fn.split('?')[0]
        fn = fn.split('#')[0]
        fn = fn.replace('%3A', ':')
        return fn in done

    if is_iterable(url):
        if directory:
            mkdir(directory)

        if skip_done:
            done = set(ls(directory))
            url = [u for u in url if not is_done(u)]

        statuses = {}
        for url_i in tqdm(url):
            statuses[url_i] = wget(url_i, directory, timeout, tries, skip_done=False, throw_error=throw_error)

        return statuses
    else:
        cmd = f'wget {url} -T {timeout} -t {tries}'
        if directory:
            # the bash "wget" command will automatically create the directory if missing
            cmd = f'{cmd} -P {directory}'
            mkdir(directory)

        if skip_done:
            cmd = f'{cmd} -N --no-if-modified-since'

        return shell(cmd, return_code=True, check=throw_error)


def get_frame_info(stack_depth=1, as_str=True):
    frame = inspect.currentframe()
    for i in range(stack_depth):
        frame = frame.f_back
    frameinfo = inspect.getframeinfo(frame)

    dt = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    filename = frameinfo.filename.split('/')[-1]
    path = frameinfo.filename
    line = frameinfo.lineno
    f = frameinfo.function

    if as_str:
        return f'[{dt} | {f}:{line}]'
    else:
        return {
            'dt': dt,
            'path': path,
            'filename': filename,
            'line': line,
            'f': f
        }
