from itertools import zip_longest


def compare_strings(actual, expected, diff_fn=None):
    if diff_fn is None:
        diff_fn = (lambda a, b: a == b)

    actual = repr(actual)
    expected = repr(expected)
    diff = ''.join('_' if diff_fn(a, b) else '^' for a, b in zip_longest(actual, expected))
    return f'\nA = {actual}\nE = {expected}\n    {diff}'


def transpose(matrix):
    if matrix and matrix[0]:
        n = len(matrix[0])
        return [[row[i] for row in matrix] for i in range(n)]
    else:
        return []
