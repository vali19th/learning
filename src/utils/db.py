import re
import traceback
from pprint import pprint

import psycopg2
from psycopg2 import sql

from . import data
from . import sys_utils
from . import files
from .data import is_iterable, is_str


def connect(login, **kwargs):
    if isinstance(login, str):
        return psycopg2.connect(login)
    elif isinstance(login, dict):
        return psycopg2.connect(**login)
    else:
        raise ValueError(f'''Case for connect({type(login)}) not covered.''')


def disconnect(connection, **kwargs):
    connection.close()


def delete_db_and_user(dbname, user, **kwargs):
    return [
        shell.sh(f'psql -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = \'{dbname}\';"'),
        shell.sh(f'psql -c "REVOKE CONNECT ON DATABASE {dbname} FROM PUBLIC;"'),
        shell.sh(f'psql -c "DROP DATABASE IF EXISTS {dbname};"'),
        shell.sh(f'psql -c "DROP USER IF EXISTS {user};"'),
    ]


def create_db_and_user(dbname, user, password, **kwargs):
    return [
        shell.sh(f'psql -c "CREATE DATABASE {dbname};"'),
        shell.sh(f'psql -c "CREATE USER {user} with encrypted password \'{password}\';"'),
        shell.sh(f'psql -c "GRANT ALL PRIVILEGES ON DATABASE {dbname} TO {user};"'),
    ]


def execute(connection, sql, args=None, *, print_sql=False, return_cursor=False, **kwargs):
    cursor = connection.cursor()
    if print_sql: print(cursor.mogrify(sql, args).decode('utf-8'))

    try:
        cursor.execute(sql, args)
        connection.commit()
        if return_cursor: return cursor
    except Exception as e:
        connection.rollback()
        cursor.close()
        raise e

    cursor.close()


def query(connection, sql, args=None, *, print_sql=False, **kwargs):
    cursor = execute(connection, sql, args, print_sql=print_sql, return_cursor=True)
    data = cursor.fetchall()
    data = [{column[0]: value for column, value in zip(cursor.description, row)} for row in (data or [])]

    cursor.close()
    return data


def query_row(connection, sql, args=None, *, print_sql=False, **kwargs):
    cursor = execute(connection, sql, args, print_sql=print_sql, return_cursor=True)
    data = cursor.fetchone()
    if data is not None:
        data = {column[0]: value for column, value in zip(cursor.description, data)}

    cursor.close()
    return data


def query_column(connection, sql, args=None, *, print_sql=False, **kwargs):
    cursor = execute(connection, sql, args, print_sql=print_sql, return_cursor=True)
    data = cursor.fetchall()
    data = [xi[0] for xi in (data or [])]

    cursor.close()
    return data


def query_value(connection, sql, args=None, *, print_sql=False, **kwargs):
    cursor = execute(connection, sql, args, print_sql=print_sql, return_cursor=True)
    data = cursor.fetchone()
    if data is not None:
        data = data[0]

    cursor.close()
    return data


def get_matching_rows(connection, row, table, select='*', print_sql=False):
    columns = ', '.join(f'{{{col}}}' for col in row.keys())
    values = ', '.join(f'%({col})s' for col in row.keys())

    if len(row) > 1:
        columns = f'({columns})'
        values = f'({values})'

    identifiers = {col: sql.Identifier(col) for col in row.keys()}
    identifiers['table'] = sql.Identifier(table)

    if select == '*':
        cols = '*'
    elif is_str(select):
        cols = f'{{{select}}}'
        identifiers[select] = sql.Identifier(select)
    elif is_iterable(select):
        cols = ', '.join(f'{{{col}}}' for col in select)
        identifiers.update({col: sql.Identifier(col) for col in select})
    else:
        raise ValueError(f'Unkown case for type {type(select)}')

    q = f"select {cols} from {{table}} where {columns} = {values}"
    return query(connection, sql.SQL(q).format(**identifiers), row, print_sql=print_sql)


def row_exists(connection, row, table, print_sql=False):
    columns = ', '.join(f'{{{k}}}' for k in row.keys())
    values = ', '.join(f'%({k})s' for k in row.keys())

    if len(row) > 1:
        columns = f'({columns})'
        values = f'({values})'

    identifiers = {k: sql.Identifier(k) for k in row.keys()}
    identifiers['table'] = sql.Identifier(table)

    q = f"select exists(select 1 from {{table}} where {columns} = {values} limit 1)"
    return query_value(connection, sql.SQL(q).format(**identifiers), row, print_sql=print_sql)


def values_in_column(connection, values, table, column, print_sql=False):
    """It intersects the column with your list and returns the result."""
    if values:
        identifiers = {'table': sql.Identifier(table), 'column': sql.Identifier(column)}
        query = sql.SQL("select distinct {column} from {table} where {column} in (select unnest(%(values)s))")
        return query_column(connection, query.format(**identifiers), {'values': values}, print_sql=print_sql)
    else:
        return []


def values_not_in_column(connection, values, table, column, print_sql=False):
    """Returns the values that don't exist in the column"""
    found = values_in_column(connection, values, table, column, print_sql=print_sql)
    return [v for v in values if v not in found]


def insert(connection, table, args, *, print_sql=False, **kwargs):
    columns = ', '.join(args)
    values = ', '.join(f'%({k})s' for k in args)

    pk = ', '.join(get_PK_columns(connection, table))
    sql = f'INSERT INTO {table} ({columns}) VALUES ({values}) RETURNING ({pk})'

    cursor = execute(connection, sql, args, print_sql=print_sql, return_cursor=True)
    generated_id = cursor.fetchone()[0]
    cursor.close()

    return generated_id


def update(connection, table, args, *, print_sql=False, **kwargs):
    pk = {k: args[k] for k in get_PK_columns(connection, table)}

    changes = ', '.join(f'{k} = %({k})s' for k in args if k not in pk)
    condition = ' and '.join(f'{k} = %({k})s' for k in pk)
    sql = f'update {table} set {changes} where {condition}'

    return execute(connection, sql, args, print_sql=print_sql, return_cursor=True)


def delete(connection, table, args, *, print_sql=False, **kwargs):
    condition = ' and '.join(f'{k} = %({k})s' for k in args)
    sql = f'delete from {table} where {condition}'

    return execute(connection, sql, args, print_sql=print_sql, return_cursor=True)


def get_PK_columns(connection, table, **kwargs):
    return query_column(connection, '''
        SELECT kc.column_name
        FROM information_schema.table_constraints tc
        JOIN information_schema.key_column_usage kc ON
                kc.table_name = tc.table_name
            AND kc.table_schema = tc.table_schema
            AND kc.constraint_name = tc.constraint_name
        WHERE   tc.constraint_type = 'PRIMARY KEY'
            AND kc.ordinal_position IS NOT NULL
            AND tc.table_name = lower(%(table)s)
        ORDER BY kc.position_in_unique_constraint
        ''', {'table': table})


def get_PK_values(pk, row, **kwargs):
    if type(pk) in (list, tuple) and len(pk) > 1:
        return tuple(get_PK_values(column, row) for column in pk)
    elif type(pk) in (list, tuple) and len(pk) == 1:
        return get_PK_values(pk[0], row)
    elif type(pk) == str and pk != '':
        return row[pk]
    else:
        raise ValueError(f'''Case for get_PK_values({pk}, {row}) not covered.''')


def order_table_definitions_by_dependency(tables, **kwargs):
    tree = {table_name: get_table_dependencies(definition) for table_name, definition in tables.items()}
    return [tables[table] for table in data.topological_sort(tree)]


def get_table_dependencies(table_definition, **kwargs):
    return set(re.findall(r'.* references (\w+) .*,', table_definition))


def create_tables(connection, path, **kwargs):
    tables = files.read_all(path, split_extension=True)
    tables = {table[0]: sql for table, sql in tables.items()} # remove the extension
    for create_table in order_table_definitions_by_dependency(tables):
        execute(connection, create_table)
