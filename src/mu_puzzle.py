from collections import defaultdict, Counter
from pprint import pprint


def main():
    transformations = defaultdict(set, {(): {'MI'}})
    rules = {
        1: rule_1,
        2: rule_2,
        3: rule_3,
        4: rule_4,
    }

    for i in range(15):
        print(i, len(transformations))
        for tr, strings in list(transformations.items()):
            if 'MU' in strings:
                print('Found solution:', tr, strings)
                break

            # print(tr)
            # print(tr, strings)
            del transformations[tr]
            for r, f in rules.items():
                for s in strings:
                    if s in ['MIU', 'MIIU']:
                        continue
                    transformations[(*tr, r)].update(f(s))


def rule_1(x):
    if x.endswith('I'):
        return {x + 'U'}
    else:
        return set()


def rule_2(x):
    return {x + x[1:]}


def rule_3(x):
    if 'III' not in x:
        return set()
    else:
        return {x[:i] + 'U' + x[i + 3:] for i in range(1, len(x)-2) if x[i:i+3] == 'III'}


def rule_4(x):
    if 'UU' not in x:
        return set()
    else:
        return {x[:i] + x[i+2:] for i in range(1, len(x) - 1) if x[i:i+2] == 'UU'}


if __name__ == '__main__':
    main()
