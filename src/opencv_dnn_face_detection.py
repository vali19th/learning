import numpy as np
import argparse
import cv2

from utils.images import load_image, show_image
from utils.opencv import process_and_show_video


def detect(image, model, min_confidence):
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    model.setInput(blob)
    detections = model.forward()

    bboxes_with_confidence = []
    for _, _, confidence, *bbox_rel in detections[0, 0]:
        if confidence >= min_confidence:
            bbox = bbox_rel * np.array([w, h, w, h])
            bboxes_with_confidence.append((bbox.astype(int), confidence))

    return bboxes_with_confidence


def draw_bboxes(bboxes_with_confidence, image):
    image = image.copy()
    for (x, y, X, Y), confidence in bboxes_with_confidence:
        text = f'{confidence * 100:.2f}%'
        y_text = y - 10 if y - 10 > 10 else y + 10

        cv2.rectangle(image, (x, y), (X, Y), (255, 0, 0), 2)
        cv2.putText(image, text, (x, y_text), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 0, 0), 2)

    return image


def detect_and_draw(image, model, confidence):
    detections = detect(image, model, confidence)
    result = draw_bboxes(detections, image)
    return result


ap = argparse.ArgumentParser()
ap.add_argument('--prototxt', type=str, default='../data/opencv_dnn_face_detection/deploy.prototxt.txt', help='path to Caffe model definition')
ap.add_argument('--model', type=str, default='../data/opencv_dnn_face_detection/res10_300x300_ssd_iter_140000.caffemodel', help='path to Caffe model weights')
ap.add_argument('--confidence', type=float, default=0.2, help='minimum probability for detection')
args = vars(ap.parse_args())

model = cv2.dnn.readNetFromCaffe(args['prototxt'], args['model'])

video = '/media/data/projects/active/polymore/data/videos/2022-12-13T07_11_09.540Z-TM-42-RTM-13-12-2022-09-10-28.mp4'
process_and_show_video(video, detect_and_draw, process_fn_args=(model, args['confidence']))
