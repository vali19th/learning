import traceback
import warnings
from importlib import import_module, resources

from importlib.metadata import version
__version__ = version(__package__)
__version_info__ = tuple(map(int, __version__.split('.')))


def get_stacktrace(e):
    return ''.join(traceback.format_exception(None, e, e.__traceback__))


def check(name, group):
    if group.endswith('*'):
        prefix = group.split('*')[0]
        return name.startswith(prefix)
    else:
        return name == group


def get_modules():
    modules = []
    for item in resources.files(__package__).iterdir():
        if item.stem == '__pycache__':
            continue

        module_name = f'{__package__}.{item.stem}'
        try:
            module = import_module(module_name)
        except Exception as e:
            warnings.warn(f'Couldn\'t import {module_name!r}\n' + get_stacktrace(e))
            continue

        modules.append(module)

    return sorted(modules, key=lambda m: m.__name__)

def get_functions_in_group(group):
    loaders = {}
    for module in get_modules():
        for func_name in dir(module):
            f = getattr(module, func_name)
            groups = getattr(f, '__katalyst_marks__', [])
            for g in groups:
                if check(g, group):
                    ext = g.rpartition('::')[2]
                    loaders[ext] = f

    # sort the dict with the most specific extension at the beginning
    # this makes it easier to pick ".tar.gz" over ".gz"
    loaders = dict(sorted(loaders.items(), key=lambda kv: len(kv[0]), reverse=True))
    return loaders


def mark(name):
    def decorator(func):
        func.__katalyst_marks__ = getattr(func, '__katalyst_marks__', [])
        func.__katalyst_marks__.append(name)
        return func

    return decorator
