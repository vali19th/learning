from katalyst.pkg import mark, get_functions_in_group
from pprint import pprint
from importlib import metadata

__version__ = metadata.version(__name__)
__version_info__ = tuple(map(int, __version__.split('.')))


def _setup_load():
    loaders = get_functions_in_group('load::*')
    _UNDEFINED = object()

    def load(path, *, load_as=None, default=_UNDEFINED, **kwargs):
        """
        Use an object instance instead of None as default value.
        This lets you use None as a meaningful default value
        that is different than "maybe undefined".
        """
        if load_as is not None:
            loader = loaders.get(load_as)
            if loader:
                return loader(path, **kwargs)
            else:
                available = ', '.join(sorted(loaders.keys()))
                raise ValueError(f'There is no loader for {load_as!r}. Try: {available}')

        for ext, loader in loaders.items():
            if path.endswith(f'.{ext}'):
                try:
                    # use try/except to avoid race conditions
                    return loader(path, **kwargs)
                except FileNotFoundError:
                    if default is _UNDEFINED:
                        raise
                    else:
                        return default

    return load


@mark('load::csv')
def load_csv(path):
    return f'load_csv("{path}")'


@mark('load::tar.gz')
def load_tar_gz(path):
    return f'load_tar_gz("{path}")'


@mark('load::gz')
def load_gz(path):
    return f'load_gz("{path}")'


load = _setup_load()
