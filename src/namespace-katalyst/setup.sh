command rm -rf **/*.egg-info
command rm -rf **/__pycache__
command rm -rf **/.pytest_cache
command rm -rf venv

deactivate
python -m venv venv
source venv/bin/activate

python -m pip install --upgrade pip pytest
python -m pip install -e katalyst/
python -m pip install -e katalyst-files/
python -m pip install -e katalyst-images/

cd  app/tests
python -m pytest
cd ../..
