from pprint import pprint

from katalyst.pkg import mark
from katalyst.files import load_csv

@mark('load::png')
@mark('load::jpg')
@mark('load::jpeg')
def load_image(path):
    return f'load_image("{path}")'

@mark('load::png.csv')
def load_image_from_csv(path):
    r = load_csv(path)
    return f'load_image_from_csv("{r}")'


from importlib.metadata import version
__version__ = version(__name__)
__version_info__ = tuple(map(int, __version__.split('.')))
