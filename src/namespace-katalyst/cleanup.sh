command rm -rf **/*.egg-info
command rm -rf **/__pycache__
command rm -rf **/.pytest_cache
command rm -rf venv

echo 'DONE'
