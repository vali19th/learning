# Try to make a process generator function that takes another function, some input data and a number of processes
# It should create N processes in bash that are calling the function with 1/N of the data passed to each
# It should then wait for the processes to send back their output
# you might need async
# The process generator function inserts some extra code (through a decorator?) in the func passed as arg to make it send back the output
# make the process generator function a decorator too!!!!!!!!!!
