def version_1():
    from serializers.json import JsonSerializer
    from serializers.xml import XmlSerializer
    from serializers.yaml import YamlSerializer

    from song import Song

    song = Song(song_id="1", title="The Same River", artist="Riverside")

    json = song.serialize(JsonSerializer())
    xml = song.serialize(XmlSerializer())
    yaml = song.serialize(YamlSerializer())

    print(f'\njson\n{json}')
    print(f'\nxml\n{xml}')
    print(f'\nyaml\n{yaml}')

    from serializers import json, xml, yaml
    print(json)
    print(xml)
    print(yaml)


def version_2():
    from serializers import factory
    from song import Song

    song = Song(song_id="1", title="The Same River", artist="Riverside")
    json = factory.serialize(song, "json")
    print(f'\njson\n{json}')

    yaml = factory.serialize(song, "yaml")
    print(f'\nyaml\n{yaml}')

    toml = factory.serialize(song, "toml")
    print(f'\ntoml\n{toml}')

version_1()
version_2()
