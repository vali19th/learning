SRC: https://realpython.com/python-import/

```bash
pip install PyYAML

cd local
python -m pip install -e .
cd ..

cd third_party
python -m pip install -e .
cd ..
```
