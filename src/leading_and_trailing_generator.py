from collections import defaultdict
from pprint import pprint
from statistics import mean

from utils.data import is_dict, is_iterable, running_window_lead_or_lag


def lagging_window(data, n, f):
    return list(running_window_lead_or_lag(data, n, wrap=None, f=f, direction='lagging'))


def leading_window(data, n, f):
    return list(running_window_lead_or_lag(data, n, wrap=None, f=f, direction='leading'))


def leading_and_trailing_window(data, *, leading=None, lagging=None):
    """
    - data format: dicts_of_lists: {'key1': [...], 'key2': [...], ...}
    - leading and trailing format: [(key1, win_size1[, wrap1[, aggregator_func1]]), ...]
        - where key1 and key2 could be duplicates in case you want to build multiple metrics based on the same column
    """
    data_leading = {f'{k}_lead {n, f.__name__}': leading_window(data[k], n, f) for k, n, f in set(leading or [])}
    data_lagging = {f'{k}_lag {n, f.__name__}': lagging_window(data[k], n, f) for k, n, f in set(lagging or [])}
    return {**data, **data_leading, **data_lagging}


def shift(data, n):
    return [data[i+n] if 0 <= i+n < len(data) else None for i in range(len(data))]


def leading_and_trailing_shift(data, *, leading=None, lagging=None):
    """
    - data format: dicts_of_lists: {'key1': [...], 'key2': [...], ...}
    - leading and trailing format: [(key1, n1), ...]
        - where key1 and key2 could be duplicates in case you want to build multiple metrics based on the same column
    """
    data_leading = {f'{k}_shift +{n}': shift(data[k], n) for k, n in set(leading or [])}
    data_lagging = {f'{k}_shift -{n}': shift(data[k], -n) for k, n in set(lagging or [])}
    return {**data, **data_leading, **data_lagging}


def cumulate(data, keys=None):
    """data is either a sequence or a dict"""
    if is_dict(data):
        if not is_iterable(keys):
            raise ValueError(f'Expected a non-empty collection of keys, got {keys!r} instead')

        missing = [k for k in keys if k not in data]
        if missing:
            raise ValueError(f'Keys not found: {missing}')

        new_data = {f'{k}_cumulative': cumulate(data[k]) for k in keys}
        return {**data, **new_data}

    if data:
        r = [data[0]]
        for x in data[1:]:
            r.append(r[-1] + x)

        return r
    else:
        return []


x = {'a': list(range(5)), 'b': list(range(10, 13))}
x = leading_and_trailing_window(x, leading=[('a', 2, sum), ('a', 3, sum)], lagging=[('a', 2, sum)])
x = leading_and_trailing_shift(x, leading=[('a', 1), ('a', 2)], lagging=[('a', 1)])
x = cumulate(x, ['a'])

pprint(x)
