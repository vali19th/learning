# Using this amazing tutorial: https://realpython.com/python-hash-table/

import copy
import itertools
from collections import deque

from pprint import pprint
from typing import NamedTuple, Any


class Pair(NamedTuple):
    key: Any
    value: Any


class HashTable:
    def __init__(self, capacity=8, load_factor_threshold=0.6):
        if not isinstance(capacity, int):
            raise TypeError(f'Capacity must be an "int". Instead got type {type(capacity).__name__!r}')

        if capacity < 1:
            raise ValueError(f'Capacity must be greater than zero. Instead got {capacity}')

        if not (0 < load_factor_threshold <= 1):
            raise ValueError("Load factor must be a number between (0, 1]")

        self._keys = []
        self._buckets = [deque() for _ in range(capacity)]
        self._load_factor_threshold = load_factor_threshold

    def __str__(self):
        return str(self.to_dict())

    def __repr__(self):
        cls = self.__class__.__name__
        return f'{cls}.from_dict({self})'

    def __len__(self):
        return len(self.pairs)

    def __delitem__(self, key):
        match self._find(key):
            case bucket, index, _:
                del bucket[index]
                self._keys.remove(key)
            case _:
                raise KeyError(key)

    def __setitem__(self, key, value):
        if self.load_factor >= self._load_factor_threshold:
            self._resize_and_rehash()

        match self._find(key):
            case deque() as bucket, index, (key, _):
                bucket[index] = Pair(key, value)
            case bucket:
                bucket.append(Pair(key, value))
                self._keys.append(key)

    def __getitem__(self, key):
        match self._find(key):
            case _, _, pair:
                return pair.value
            case _:
                raise KeyError(key)

    def __contains__(self, key):
        try:
            self[key]
            return True
        except KeyError:
            return False

    def __iter__(self):
        yield from self.keys

    def __eq__(self, other):
        if self is other:
            return True

        if type(self) is not type(other):
            return False

        return set(self.pairs) == set(other.pairs)

    def __or__(self, other):
        new = self.copy()
        new.update(other)
        return new

    def __ror__(self, other):
        """This is called only when "other" is an object of a different type"""
        new = self.from_dict(other)
        new.update(self)
        return new

    @classmethod
    def from_dict(cls, d, capacity=None):
        if capacity is None:
            capacity = len(d)

        obj = cls(capacity)
        for k, v in d.items():
            obj[k] = v

        return obj

    @property
    def pairs(self):
        return [(k, self[k]) for k in self.keys]

    @property
    def values(self):
        return [self[k] for k in self.keys]

    @property
    def keys(self):
        return copy.deepcopy(self._keys)

    @property
    def capacity(self):
        return len(self._buckets)

    @property
    def load_factor(self):
        return len(self) / self.capacity

    def to_dict(self):
        return dict(self.pairs)

    def copy(self):
        return HashTable.from_dict(dict(self.pairs), self.capacity)

    def clear(self):
        for k in self.keys:
            del self[k]

    def update(self, data):
        for k in data:
            self[k] = data[k]

    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default

    def _index_of(self, key):
        return hash(key) % self.capacity

    def _resize_and_rehash(self):
        copy = HashTable(capacity=self.capacity * 2)
        for k, v in self.pairs:
            copy[k] = v

        self._buckets = copy._buckets

    def _find(self, key):
        bucket = self._buckets[self._index_of(key)]
        for index, pair in enumerate(bucket):
            if pair.key == key:
                return bucket, index, pair

        return bucket
