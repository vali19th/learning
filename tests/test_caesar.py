import string
from pprint import pprint

from caesar import caesar


def test_empty():
    assert caesar('', 0) == ''
    assert caesar('', 1) == ''


def test_ignore_non_letters():
    assert caesar('1, 2, 3!', 5) == '1, 2, 3!'


def test_n_equals_0():
    assert caesar('hello', 0) == 'hello'


def test_n_is_negative():
    assert caesar('hello', -1) == 'gdkkn'


def test_wrap_around():
    assert caesar('abcd xyz', 1) == 'bcde yza'
    assert caesar('abcd xyz', -1) == 'zabc wxy'
    assert caesar('abcd xyz', 26) == 'abcd xyz'
