from pprint import pprint

import pytest

from sudoku import _get_boxes, _get_cols, _get_rows, _postconditions, get_grid, sudoku
from utils.misc import compare_strings
from utils.test import raises

ex1 = {
    'problem': '85...24..72......9..4.........1.7..23.5...9...4...........8..7..17..........36.4.',
    'solution': '859612437723854169164379528986147352375268914241593786432981675617425893598736241',
    'rows': ['859612437', '723854169', '164379528', '986147352', '375268914', '241593786', '432981675', '617425893', '598736241'],
    'cols': ['871932465', '526874319', '934651278', '683125947', '157469823', '249783156', '415397682', '362518794', '798246531'],
    'boxes': ['859723164', '986375241', '432617598', '612854379', '147268593', '981425736', '437169528', '352914786', '675893241'],
    'grid': '8 5 . | . . 2 | 4 . .\n7 2 . | . . . | . . 9\n. . 4 | . . . | . . .\n------+------+------\n. . . | 1 . 7 | . . 2\n3 . 5 | . . . | 9 . .\n. 4 . | . . . | . . .\n------+------+------\n. . . | . 8 . | . 7 .\n. 1 7 | . . . | . . .\n. . . | . 3 6 | . 4 .',
}

ex2 = {
    'problem': '4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......',
    'solution': '417369825632158947958724316825437169791586432346912758289643571573291684164875293',
    'rows': ['417369825', '632158947', '958724316', '825437169', '791586432', '346912758', '289643571', '573291684', '164875293'],
    'cols': ['469873251', '135294876', '728516934', '317459628', '652381497', '984762315', '893147562', '241635789', '576928143'],
    'boxes': ['417632958', '369158724', '825947316', '825791346', '437586912', '169432758', '289573164', '643291875', '571684293'],
    'grid': '4 . . | . . . | 8 . 5\n. 3 . | . . . | . . .\n. . . | 7 . . | . . .\n------+------+------\n. 2 . | . . . | . 6 .\n. . . | . 8 . | 4 . .\n. . . | . 1 . | . . .\n------+------+------\n. . . | 6 . 3 | . 7 .\n5 . . | 2 . . | . . .\n1 . 4 | . . . | . . .',
}


@pytest.mark.parametrize('ex', [ex1, ex2])
def test_get_grid(ex):
    grid = get_grid(ex['problem'])
    assert grid == ex['grid'], compare_strings(grid, ex['grid'])


def test_input_is_str():
    e = TypeError(f'Expected a str, got "NoneType" instead')
    raises(e, sudoku, None)

    e = TypeError(f'Expected a str, got "list" instead')
    raises(e, sudoku, [])


def test_input_has_81_chars():
    e = ValueError(f'Expected 81 dots and digits, got 80 instead')
    raises(e, sudoku, '.' * 80)

    e = ValueError(f'Expected 81 dots and digits, got 82 instead')
    raises(e, sudoku, '.' * 82)


def test_input_is_all_digits_and_dots():
    chars = 'ab4;.6.79......6.2.56.923...78 61.3.5.9...4.6.2.54.89...741.92.1.5......84.6..1..'
    mistakes = '[a][b]4[;].6.79......6.2.56.923...78[ ]61.3.5.9...4.6.2.54.89...741.92.1.5......84.6..1..'
    e = ValueError(f'Expected only dots and digits, got "{mistakes}" instead')
    raises(e, sudoku, chars)

    chars = '004! 6?79000000602056092300078061030509000406020540890007x10920105000000840600100'
    mistakes = '004[!][ ]6[?]79000000602056092300078061030509000406020540890007[x]10920105000000840600100'
    e = ValueError(f'Expected only dots and digits, got "{mistakes}" instead')
    raises(e, sudoku, chars)


@pytest.mark.parametrize('ex', [ex1, ex2])
def test_output_is_not_broken(ex):
    solution = sudoku(ex['problem'])
    _postconditions(ex['problem'], solution)
