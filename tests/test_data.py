from utils.data import as_dict_of_lists, as_list_of_dicts


def test_as_list_of_dicts():
    assert as_list_of_dicts([]) == []
    assert as_dict_of_lists({}) == {}

    x1 = {'a': [1, 2]}
    y1 = [{'a': 1}, {'a': 2}]
    assert as_list_of_dicts(x1) == y1, as_list_of_dicts(x1)
    assert as_list_of_dicts(y1) == y1, as_list_of_dicts(y1)

    assert as_dict_of_lists(y1) == x1, as_dict_of_lists(y1)
    assert as_dict_of_lists(x1) == x1, as_dict_of_lists(x1)

    x2 = {'a': [1, 2], 'b': [True, False]}
    y2 = [{'a': 1, 'b': True}, {'a': 2, 'b': False}]
    assert as_list_of_dicts(x2) == y2, as_list_of_dicts(x2)
    assert as_list_of_dicts(y2) == y2, as_list_of_dicts(y2)

    assert as_dict_of_lists(y2) == x2, as_dict_of_lists(y2)
    assert as_dict_of_lists(x2) == x2, as_dict_of_lists(x2)
