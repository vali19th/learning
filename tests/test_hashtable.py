import pytest

from collections import deque
from unittest.mock import patch


from src.hashtable import HashTable
from utils.test import check_error, raises


@pytest.fixture
def hash_table():
    sample_data = HashTable(capacity=100)
    sample_data['hola'] = 'hello'
    sample_data[98.6] = 37
    sample_data[False] = True
    return sample_data


def test_clear(hash_table):
    hash_table.clear()
    assert len(hash_table) == 0
    assert hash_table.capacity == 100

    assert hash_table.keys == []
    assert hash_table.values == []
    assert hash_table.keys == []


def test_update():
    hash_table = HashTable.from_dict({'a': 1, 'b': 2})
    hash_table.update({'b': -2, 'c': 3})
    assert hash_table == HashTable.from_dict({'a': 1, 'b': -2, 'c': 3})


def test_union_operator(hash_table):
    d1 = {'a': 1, 'b': 2}
    d2 = {'b': -2, 'c': 3}
    h1 = HashTable.from_dict(d1)
    h2 = HashTable.from_dict(d2)

    h12 = HashTable.from_dict({'a': 1, 'b': -2, 'c': 3})
    h21 = HashTable.from_dict({'a': 1, 'b': 2, 'c': 3})

    # with dict
    assert h1 | d1 == h1
    assert h1 | d2 == h12
    assert h2 | d1 == h21
    assert d1 | h2 == h12
    assert d2 | h1 == h21

    # with HashTable
    assert h1 | h1 == h1
    assert h1 | h2 == h12
    assert h2 | h1 == h21


def test_invalid_type_for_capacity():
    e = TypeError('Capacity must be an "int". Instead got type "float"')
    raises(e, HashTable, capacity=0.0)

    e = TypeError('Capacity must be an "int". Instead got type "str"')
    raises(e, HashTable, capacity='0.0')

    e = TypeError('Capacity must be an "int". Instead got type "list"')
    raises(e, HashTable, capacity=[0])


def test_create_hashtable():
    assert HashTable(capacity=2) is not None


def test_report_capacity():
    assert HashTable(capacity=2).capacity == 2


def test_insert_key_value_pairs(hash_table):
    assert ('hola', 'hello') in hash_table.pairs
    assert (98.6, 37) in hash_table.pairs
    assert (False, True) in hash_table.pairs


def test_insert_does_not_change_capacity():
    hash_table = HashTable(capacity=2)
    hash_table['hola'] = 'hello'

    assert hash_table.capacity == 2


def test_create_hashtable_with_default_load_factor_threshold():
    assert HashTable()._load_factor_threshold == 0.6


def test_should_not_create_hashtable_with_zero_load_factor_threshold():
    e = ValueError('Load factor must be a number between (0, 1]')
    raises(e, HashTable, load_factor_threshold=0)


def test_should_not_create_hashtable_with_negative_load_factor_threshold():
    e = ValueError('Load factor must be a number between (0, 1]')
    raises(e, HashTable, load_factor_threshold=-100)


def test_create_hashtable_with_one_load_factor_threshold():
    assert HashTable(load_factor_threshold=1)._load_factor_threshold == 1


def test_should_not_create_hashtable_with_load_factor_threshold_above_one():
    e = ValueError('Load factor must be a number between (0, 1]')
    raises(e, HashTable, load_factor_threshold=1.1)


def test_length_after_init():
    hash_table = HashTable(capacity=1)
    assert len(hash_table) == 0


def test_length_after_insert():
    hash_table = HashTable(capacity=1)
    hash_table['hola'] = 'hello'
    assert len(hash_table) == 1


def test_length_after_update():
    hash_table = HashTable(capacity=1)
    hash_table['hola'] = 'hello'
    hash_table['hola'] = 'saluton'
    assert len(hash_table) == 1


def test_length_after_delete():
    hash_table = HashTable(capacity=1)
    hash_table['hola'] = 'hello'
    del hash_table['hola']
    assert len(hash_table) == 0


def test_capacity_of_empty_hash_table():
    assert HashTable(capacity=100).capacity == 100


def test_capacity(hash_table):
    assert hash_table.capacity == 100


def test_create_empty_buckets():
    assert HashTable(capacity=3)._buckets == [deque(), deque(), deque()]


def test_should_not_shrink_when_removing_elements(hash_table):
    del hash_table['hola']
    assert hash_table.capacity == 100


def test_should_not_contain_none_value_when_created():
    assert None not in HashTable(capacity=100).values


def test_insert_none_value():
    hash_table = HashTable(capacity=1)
    hash_table['key'] = None
    assert ('key', None) in hash_table.pairs


def test_find_value_by_key(hash_table):
    assert hash_table['hola'] == 'hello'
    assert hash_table[98.6] == 37
    assert hash_table[False] is True


def test_raise_error_on_missing_key(hash_table):
    with pytest.raises(KeyError) as e:
        _ = hash_table['missing_key']
    check_error(e, 'missing_key')


def test_find_key(hash_table):
    assert 'hola' in hash_table


def test_should_not_find_key(hash_table):
    assert 'missing_key' not in hash_table


def test_get_value(hash_table):
    assert hash_table.get('hola') == 'hello'


def test_get_none_when_missing_key(hash_table):
    assert hash_table.get('missing_key') is None


def test_get_default_value_when_missing_key(hash_table):
    assert hash_table.get('missing_key', 'default') == 'default'


def test_get_value_with_default(hash_table):
    assert hash_table.get('hola', 'default') == 'hello'


def test_delete_key_value_pair(hash_table):
    assert 'hola' in hash_table
    assert ('hola', 'hello') in hash_table.pairs

    del hash_table['hola']

    assert 'hola' not in hash_table
    assert ('hola', 'hello') not in hash_table.pairs


def test_raise_key_error_when_deleting(hash_table):
    with pytest.raises(KeyError) as e:
        del hash_table['missing_key']
    check_error(e, 'missing_key')


def test_update_value(hash_table):
    assert hash_table['hola'] == 'hello'

    hash_table['hola'] = 'hallo'

    assert hash_table['hola'] == 'hallo'
    assert hash_table[98.6] == 37
    assert hash_table[False] is True


def test_return_pairs(hash_table):
    assert hash_table.pairs == [("hola", "hello"), (98.6, 37), (False, True)]


def test_get_pairs_of_empty_hash_table():
    assert HashTable(capacity=100).pairs == []


def test_return_copy_of_pairs(hash_table):
    assert hash_table.pairs is not hash_table.pairs


def test_not_include_blank_pairs(hash_table):
    assert None not in hash_table.pairs


def test_return_duplicate_values():
    hash_table = HashTable(capacity=100)
    hash_table["Alice"] = 24
    hash_table["Bob"] = 42
    hash_table["Joe"] = 42
    assert [24, 42, 42] == sorted(hash_table.values)


def test_get_values(hash_table):
    assert hash_table.values == ["hello", 37, True]


def test_get_values_of_empty_hash_table():
    assert HashTable(capacity=1).values == []


def test_return_copy_of_values(hash_table):
    assert hash_table.values is not hash_table.values


def test_get_keys(hash_table):
    assert hash_table.keys == ["hola", 98.6, False]


def test_get_keys_of_empty_hash_table():
    assert HashTable(capacity=1).keys == []


def test_return_copy_of_keys(hash_table):
    assert hash_table.keys is not hash_table.keys


def test_convert_to_dict(hash_table):
    d = {'hola': 'hello', 98.6: 37, False: True}

    assert hash_table.to_dict() == d
    assert dict(hash_table.pairs) == d


def test_create_hashtable_with_default_capacity():
    assert HashTable().capacity == 8


def test_should_not_create_hashtable_with_zero_capacity():
    e = ValueError('Capacity must be greater than zero. Instead got 0')
    raises(e, HashTable, capacity=0)


def test_should_not_create_hashtable_with_negative_capacity():
    e = ValueError('Capacity must be greater than zero. Instead got -100')
    raises(e, HashTable, capacity=-100)


def test_iterate_over_keys(hash_table):
    for key in hash_table.keys:
        assert key in ("hola", 98.6, False)


def test_iterate_over_values(hash_table):
    for value in hash_table.values:
        assert value in ("hello", 37, True)


def test_iterate_over_pairs(hash_table):
    for key, value in hash_table.pairs:
        assert key in hash_table.keys
        assert value in hash_table.values


def test_iterate_over_instance(hash_table):
    for key in hash_table:
        assert key in ("hola", 98.6, False)


def test_use_dict_literal_for_str(hash_table):
    assert str(hash_table) in {
        "{'hola': 'hello', 98.6: 37, False: True}",
        "{'hola': 'hello', False: True, 98.6: 37}",
        "{98.6: 37, 'hola': 'hello', False: True}",
        "{98.6: 37, False: True, 'hola': 'hello'}",
        "{False: True, 'hola': 'hello', 98.6: 37}",
        "{False: True, 98.6: 37, 'hola': 'hello'}",
    }


def test_create_hashtable_from_dict():
    dictionary = {"hola": "hello", 98.6: 37, False: True}
    hash_table = HashTable.from_dict(dictionary)

    assert hash_table.capacity == 2 * len(dictionary)
    assert hash_table.keys == list(dictionary.keys())
    assert hash_table.pairs == list(dictionary.items())
    assert hash_table.values == list(dictionary.values())


def test_create_hashtable_from_dict_with_custom_capacity():
    dictionary = {"hola": "hello", 98.6: 37, False: True}
    hash_table = HashTable.from_dict(dictionary, capacity=100)

    assert hash_table.capacity == 100
    assert hash_table.keys == list(dictionary.keys())
    assert hash_table.pairs == list(dictionary.items())
    assert hash_table.values == list(dictionary.values())


def test_has_canonical_string_representation(hash_table):
    assert repr(hash_table) in {
        "HashTable.from_dict({'hola': 'hello', 98.6: 37, False: True})",
        "HashTable.from_dict({'hola': 'hello', False: True, 98.6: 37})",
        "HashTable.from_dict({98.6: 37, 'hola': 'hello', False: True})",
        "HashTable.from_dict({98.6: 37, False: True, 'hola': 'hello'})",
        "HashTable.from_dict({False: True, 'hola': 'hello', 98.6: 37})",
        "HashTable.from_dict({False: True, 98.6: 37, 'hola': 'hello'})",
    }


def test_equal_to_itself(hash_table):
    assert hash_table == hash_table


def test_equal_to_copy(hash_table):
    assert hash_table is not hash_table.copy()
    assert hash_table == hash_table.copy()


def test_equal_different_key_value_order(hash_table):
    h1 = HashTable.from_dict({"a": 1, "b": 2, "c": 3})
    h2 = HashTable.from_dict({"b": 2, "a": 1, "c": 3})
    assert h1 == h2


def test_unequal(hash_table):
    other = HashTable.from_dict({"different": "value"})
    assert hash_table != other


def test_unequal_another_data_type(hash_table):
    assert hash_table != 42


def test_copy_keys_values_pairs_capacity(hash_table):
    copy = hash_table.copy()
    assert copy is not hash_table
    assert hash_table.keys == copy.keys
    assert hash_table.values == copy.values
    assert hash_table.pairs == copy.pairs
    assert hash_table.capacity == copy.capacity


def test_equal_different_capacity():
    data = {"a": 1, "b": 2, "c": 3}
    h1 = HashTable.from_dict(data, capacity=50)
    h2 = HashTable.from_dict(data, capacity=100)
    assert h1 == h2


@patch("builtins.hash", return_value=24)
def test_detect_and_resolve_hash_collisions(_):
    hash_table = HashTable(capacity=100)
    hash_table["hola"] = "hello"
    hash_table[98.6] = 37

    assert len(hash_table) == 2
    assert hash_table.pairs == [("hola", "hello"), (98.6, 37)]


def test_double_capacity():
    hash_table = HashTable(capacity=3)
    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    hash_table["gracias"] = "thank you"

    assert len(hash_table) == 4
    assert hash_table.capacity == 6
    assert dict(hash_table.pairs) == {
        "hola": "hello",
        98.6: 37,
        False: True,
        "gracias": "thank you",
    }


@patch("builtins.hash", return_value=24)
def test_get_collided_values(_):
    hash_table = HashTable(capacity=3)
    hash_table["hola"] = "hello"
    hash_table[98.6] = 37
    hash_table[False] = True

    assert len(hash_table) == 3
    assert hash_table["hola"] == "hello"
    assert hash_table[98.6] == 37
    assert hash_table[False] is True


def test_pairs_should_not_contain_deleted(hash_table):
    del hash_table["hola"]
    del hash_table[98.6]

    assert hash_table.pairs == [(False, True)]


def test_load_factor(hash_table):
    assert hash_table.load_factor == 3 / 100


def test_include_only_buckets_in_load_factor(hash_table):
    del hash_table["hola"]
    del hash_table[98.6]
    assert hash_table.load_factor == 1 / 100


def test_resize_eagerly_when_load_factor_equal_or_greater():
    hash_table = HashTable(capacity=100, load_factor_threshold=0.5)
    for i in range(50):
        hash_table[i] = "value"

    assert len(hash_table) == 50
    assert hash_table.capacity == 100
    assert hash_table.load_factor == 0.5

    hash_table[51] = "value"

    assert len(hash_table) == 51
    assert hash_table.capacity == 200
    assert hash_table.load_factor == 51 / 200
