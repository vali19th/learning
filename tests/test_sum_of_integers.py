from sum_of_integers import add_it_up
# Write a function, add_it_up(), that takes a single integer as input and returns the sum of the integers from zero to the input parameter.
# The function should return 0 if a non-integer is passed in.


def test_0_if_non_int():
    assert add_it_up(None) == 0
    assert add_it_up(True) == 0
    assert add_it_up('hi') == 0
    assert add_it_up('1') == 0
    assert add_it_up(1.0) == 0
    assert add_it_up([]) == 0


def test_sum_up_to_n():
    assert add_it_up(0) == 0
    assert add_it_up(1) == 1
    assert add_it_up(5) == 15
    assert add_it_up(10) == 55

    assert add_it_up(-1) == -1
    assert add_it_up(-5) == -15
    assert add_it_up(-10) == -55
