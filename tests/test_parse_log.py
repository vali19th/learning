import pytest

from parse_log import parse_log
from utils.test import raises


def test_empty_log():
    assert parse_log('') == ''


def test_ignored_states():
    assert parse_log('10:30:00: Device State: SENTIENT') == ''


def test_bad_format():
    log = 'gibberish'
    e = ValueError(f'Unrecognized format: {log!r}')
    raises(e, parse_log, log)

    log = '10:30:00'
    e = ValueError(f'Unrecognized format: {log!r}')
    raises(e, parse_log, log)

    log = 'Device State: ON'
    e = ValueError(f'Unrecognized format: {log!r}')
    raises(e, parse_log, log)

    log = '2022/12/01 10:30:00: Device State: ON'
    e = ValueError(f'Unrecognized format: {log!r}')
    raises(e, parse_log, log)

def test_only_ON():
    expected = f'The device has been running without interruption since 10:30:00'
    assert parse_log('10:30:00: Device State: ON') == expected


def test_should_start_as_ON():
    log = '\n'.join([
        '10:30:00: Device State: OFF',
        '10:30:00: Device State: ON',
    ])
    e = ValueError(f'The initial state must always be "ON", instead got "OFF"')
    raises(e, parse_log, log)


def test_should_find_errors():
    log = '\n'.join([
        '10:30:00: Device State: ON',
        '10:30:15: Device State: ERR',
        '12:30:15: Device State: ERR',
        '14:30:15: Device State: ERR',
    ])

    expected = '\n'.join([
        'The device has been running without interruption since 10:30:00',
        'Timestamps of error events:',
        '    10:30:15',
        '    12:30:15',
        '    14:30:15',
    ])

    assert parse_log(log) == expected


def test_should_alternate_between_ON_and_OFF():
    log = '\n'.join([
        '10:30:00: Device State: ON',
        '12:30:15: Device State: ON',
    ])
    e = ValueError('Found two consecutive "ON" toggles at 10:30:00 and 12:30:15')
    raises(e, parse_log, log)

    log = '\n'.join([
        '10:30:00: Device State: ON',
        '12:30:15: Device State: OFF',
        '12:31:15: Device State: OFF',
    ])
    e = ValueError('Found two consecutive "OFF" toggles at 12:30:15 and 12:31:15')
    raises(e, parse_log, log)


def test_should_calculate_duration():
    log = '\n'.join([
        '10:30:00: Device State: ON',
        '10:30:15: Device State: OFF',
        '10:30:15: Device State: ON',
        '10:32:15: Device State: OFF',
    ])

    assert parse_log(log) == 'Device was on for 135s'
